<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $title       = "Admin";
    public $active      = "dashboard";
    public $template    = "template/admin";

    public function admin_template($content, $data, $script = "script")
    {
        $template_data = array("title"=>$this->title, "active"=>$this->active);
        $data = array(
                "html_header"   =>  $this->load->view($this->template.'/header', $template_data, true),
                "html_navbar"   =>  $this->load->view($this->template.'/navbar', null, true),
                "html_menu"     =>  $this->load->view($this->template.'/menu', $template_data, true),
                "html_body"     =>  $this->load->view($this->module."/".$content,$data, true),
                "html_footer"   =>  $this->load->view($this->template.'/footer', null, true),
                "script"        =>  $this->load->view($this->module."/".$script, null, true)
            );

        $this->load->view($this->template.'/index',$data);
    }

        public function admin_template_UIR($content, $data, $script = "script", $form_action)
    {
        $button_name;
        $form_name;
        if ($form_action == "SAVE")
        {
            $button_name = "SEND";
            $form_name = "NEW UIR";
        }else
        {
            $button_name = "UPDATE";
            $form_name = "UPDATE UIR";
        }
        $template_data = array("title"=>$this->title, "active"=>$this->active);
        $data = array(
                "html_header"   =>  $this->load->view($this->template.'/header', $template_data, true),
                "html_navbar"   =>  $this->load->view($this->template.'/navbar', null, true),
                "html_menu"     =>  $this->load->view($this->template.'/menu', $template_data, true),
                "html_body"     =>  $this->load->view($this->module."/".$content,$data, true),
                "html_body"     =>  $this->load->view($this->module."/".$content,array('form_type' => $form_name,'button_name' => $button_name), true),
                "html_footer"   =>  $this->load->view($this->template.'/footer', null, true),
                "script"        =>  $this->load->view($this->module."/".$script, null, true)
            );

        $this->load->view($this->template.'/index',$data);
    }


    
    public function search($offset=0)
    {
        $this->load->model($this->model);

        $where = $this->input->post("where");
        $order = $this->input->post("order");
        $by = "DESC";

        $data_view['records'] = $this->{$this->model}->get_paginate($where, $this->limit, $offset, $order, $by);

        $data_view['count'] = $this->{$this->model}->num_rows();

        //pagination
        $config['base_url'] = site_url() . $this->module . '/' . $this->method . '/';
        $config['total_rows'] = $data_view['count'];
        $config['per_page'] = $this->limit;
        $config['uri_segment'] = 3;

        $data_view['pagination'] = $this->_gen_pagination($config);

        // $this->load->view($this->module . '/index', $data_view);
        $this->admin_template('/index', $data_view, "script");
    }

    public function _gen_pagination($config)
    {
        $config['first_link'] = '&lsaquo; First';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['last_link'] = 'Last &rsaquo;';
        $config['full_tag_open'] = '<div class="pagination"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_tag_open'] = '<li class="first">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="last">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="previous">';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['first_link'] = 'First';

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        return $this->pagination->create_links();
    }
}
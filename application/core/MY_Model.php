<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {

    public $CI;

    public function __construct()
    {
        $this->CI = & get_instance();
        $this->CI->load->database();
        $this->CI->load->dbutil();
    }

    public function add($data)
    {
        return $this->CI->db->insert($this->table, $data);
    }

    public function add_batch($data)
    {
        return $this->CI->db->insert_batch($this->table, $data);
    }

    public function edit($where, $data)
    {
        $this->CI->db->where($where);
        return $this->CI->db->update($this->table, $data);
    }

    public function delete($where)
    {
        return $this->CI->db->delete($this->table, $where);
    }

    public function get($where)
    {
        return $this->CI->db->get_where($this->table, $where);
    }
    public function get_like($like,$where)
    {
        $this->CI->db->select('*');
        $this->CI->db->from($this->table);
        $this->CI->db->where($where);
        $this->CI->db->like($like);
        return $this->CI->db->get();
    }

    public function all($limit = FALSE, $order = FALSE, $by = FALSE,$where = FALSE)
    {
        if($where !== FALSE)
            $this->CI->db->where($where);
        if($limit !== FALSE)
            $this->CI->db->limit($limit);
        if($order !== FALSE)
            $this->CI->db->order_by($order, $by);
        return $this->CI->db->get($this->table);
    }

    public function num_rows()
    {
        $result = $this->CI->db->get($this->table);
        return $result->num_rows;
    }
    public function get_join($where,$join,$select)
    {
        $this->CI->db->select($select);
        $this->CI->db->from($this->table);
        $this->CI->db->where($where);
        $this->CI->db->join($join[0],$join[1]);
        return $this->CI->db->get();
    }
    public function get_distinct($select,$where)
    {
        $this->CI->db->select($select);
        $this->CI->db->from($this->table ." A");
        $this->CI->db->join("uir_new B", "A.uir_ticket_no = B.uir_ticket_no","INNER");
        $this->CI->db->join("incident C", "B.incident_code = C.incident_code","INNER");
        $this->CI->db->join("vtype D", "C.vtype_main_code = D.main_code","INNER");
        $this->CI->db->where($where);
       // $this->CI->db->order_by("A.label","ASC");
        return $this->CI->db->get();
    }
    public function distinct_field($select,$where)
    {
        $this->CI->db->distinct();
        $this->CI->db->select($select);
        $this->CI->db->from($this->table);
        if($where != false){
        $this->CI->db->where($where);
        return $this->CI->db->get();
        }
    }

    public function get_paginate($where=FALSE, $limit=10, $offset=0, $order=FALSE, $by='DESC')
    {
        if($where !== FALSE)
            $this->CI->db->where($where);
        $this->db->limit($limit, $offset);
        if($order !== FALSE)
            $this->CI->db->order_by($order, $by);
        return $this->CI->db->get($this->table);
    }

}
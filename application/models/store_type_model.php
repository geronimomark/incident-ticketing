<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_type_model extends MY_Model {

    public $table = "store_type";

    public function __construct()
    {
        parent::__construct();
    }
    
}
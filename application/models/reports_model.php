<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends MY_Model {

    public $table = "reports";

    public function __construct()
    {
        parent::__construct();
    }
    
}
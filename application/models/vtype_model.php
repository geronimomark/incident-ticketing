<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vtype_model extends MY_Model {

    public $table = "vtype";

    public function __construct()
    {
        parent::__construct();
    }

    public function get_stats($mode = "ALL")
    {
    	$this->db->select("A.main_name, COUNT(C.incident_code) AS TOTAL");
    	$this->db->from($this->table . " A");
    	$this->db->join("incident B", "A.main_code = B.vtype_main_code", "INNER");
    	if($mode === "MONTHLY") {
    		$start = date("Y-m-01");
    		$end = date("Y-m-t");
    		$this->db->join("uir_new C", "B.incident_code = C.incident_code AND DATE(C.last_update) BETWEEN '{$start}' AND '{$end}' AND C.remove = '0'", "LEFT");
    	} elseif($mode === "DAILY") {
    		$now = date("Y-m-d");
    		$this->db->join("uir_new C", "B.incident_code = C.incident_code AND DATE(C.last_update) = '{$now}' AND C.remove = '0'" , "LEFT");
		} else {
    		$this->db->join("uir_new C", "B.incident_code = C.incident_code AND C.remove = '0'", "LEFT");	
    	}
    	
    	$this->db->group_by('A.main_code');
    	return $this->db->get();
    }

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Uir_new_model extends MY_Model {

    public $table = "uir_new";

    public function __construct()
    {
        parent::__construct();
    }

    public function count_uir_today()
    {
    	$start 	= date("Y-m-d") . " 00:00:00";
    	$end 	= date("Y-m-d") . " 23:59:59";
    	$this->db->select("COUNT(*) as 'COUNT'");
    	$this->db->from($this->table);
        $this->db->where("remove","0");
    	$this->db->where("`last_update` BETWEEN '{$start}' AND '{$end}'");
    	return $this->db->get();
    }

    public function get_uir($like = FALSE, $limit = FALSE)
    {
        $this->db->select("B.`incident_name`,B.uir_code, B.security_code, C.main_name, D.store_name, D.pc_code, D.name_code, E.mic_name, F.sg_name, A.*");
        $this->db->from($this->table . " A");
        $this->db->join("incident B", "B.incident_code = A.incident_code", "INNER");
        $this->db->join("vtype C", "B.vtype_main_code = C.main_code", "INNER");
        $this->db->join("store D", "D.store_code = A.store_name_code", "INNER");
        $this->db->join("mic E", "E.mic_code = A.mic_code", "INNER");
        $this->db->join("sg F", "F.sg_code = A.sg_code", "INNER");
        $this->db->where("A.remove","0");
        if(! $like) {
            $this->db->where("DATE(A.last_update)", date("Y-m-d"));
           
            $this->db->order_by("A.last_update", "DESC");
            if($limit) {
                $this->db->limit("5");
            }
        } else {

            $this->db->order_by("A.last_update", "DESC");
            $this->db->like($like["field"], $like["search"]);
        }
        return $this->db->get();
    }

    public function get_view($where)
    {
        $this->db->select("B.`incident_name`,B.uir_code, B.security_code, C.main_name,C.main_code, D.store_name, D.pc_code, D.name_code, E.mic_name, F.sg_name, G.`label`, G.`value`, A.*");
        $this->db->from($this->table . " A");
        $this->db->join("incident B", "B.incident_code = A.incident_code", "INNER");
        $this->db->join("vtype C", "B.vtype_main_code = C.main_code", "INNER");
        $this->db->join("store D", "D.store_code = A.store_name_code", "INNER");
        $this->db->join("mic E", "E.mic_code = A.mic_code", "INNER");
        $this->db->join("sg F", "F.sg_code = A.sg_code", "INNER");
        $this->db->join("uir_details G", "G.uir_ticket_no = A.uir_ticket_no", "LEFT");
        $this->db->where($where);
        $this->db->where("A.remove","0");
        $this->db->order_by("D.pc_code,C.main_name,B.incident_code,A.uir_ticket_no,A.uir_date", "ASC");
        return $this->db->get();
    }

    public function count($mode = "ALL")
    {
        if($mode === "MONTHLY") {
            $start = date("Y-m-01");
            $end = date("Y-m-t");
            $this->db->where("REMOVE = '0'");
            $this->db->where("DATE(last_update) BETWEEN '{$start}' AND '{$end}'");
        } elseif($mode === "DAILY") {
            $now = date("Y-m-d");
            $this->db->where("REMOVE = '0'");
            $this->db->where("DATE(last_update) = '{$now}'");
        }
        return $this->db->count_all_results($this->table);
    }
    public function get_uir_otherfield($where){
        $this->db->select("A.*");
        $this->db->from("uir_details A");
        $this->db->where($where);
        return $this->db->get();
    }
// SELECT B.`v_name`, C.main_name, D.store_name, E.mic_name, F.sg_name, G.`label`, G.`value`, 
// A.* 
// FROM uir_new A 
// INNER JOIN vclass B 
// ON B.v_code = A.incident_code
// INNER JOIN vtype C 
// ON B.main_code = C.main_code
// INNER JOIN store D 
// ON D.store_code = A.store_name_code
// INNER JOIN mic E 
// ON E.mic_code = A.mic_code
// INNER JOIN sg F 
// ON F.sg_code = A.sg_code
// LEFT JOIN uir_details G 
// ON G.uir_ticket_no = A.uir_ticket_no
    
}
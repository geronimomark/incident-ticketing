<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recipient_model extends MY_Model {

    public $table = "recipient";

    public function __construct()
    {
        parent::__construct();
    }
    public function get_emails_recipient()
    {
        $this->db->select("email");
        $this->db->from($this->table . " R");
        $this->db->WHERE("R.color like 'RED' AND R.status <> 0");
        return $this->db->get();
    }

}
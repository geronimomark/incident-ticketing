<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Incident_model extends MY_Model {

    public $table = "incident";

    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_with_class($where)
    {
    	$this->db->select('A.*, B.main_name');
    	$this->db->from('incident A');
    	$this->db->join('`vtype` B', 'A.vtype_main_code = B.main_code', 'INNER');
    	$this->db->where($where);
    	return $this->db->get();
    }
    
}
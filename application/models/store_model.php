<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_model extends MY_Model {

    public $table = "store";

    public function __construct()
    {
        parent::__construct();
    }

    public function get_last_store()
    {
    	$this->db->select("store_code");
    	$this->db->from($this->table);
    	$this->db->order_by("store_code", "DESC");
    	$this->db->limit("1");
    	return $this->db->get();
    }
    
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store extends MY_Controller {

    public $model = "store_model";
    public $module = "store";
    public $method = "";
    public $limit = 10;

    public function __construct()
    {
        parent::__construct();
        $this->title  = "STORE MAINTENANCE";
        $this->active = "maintenance";
        if($_POST && ! Sess::check_session()) {
            echo json_encode(array("error"=>"Session Expired"));
            exit();
        }
        if(! Sess::check_session()) {
            redirect(base_url()."login");
        }
        $this->load->model($this->model);
    }

    public function index()
    {
        
        $this->load->model("store_type_model");
        $data["store_type"] = $this->store_type_model->all()->result_object(); //load store type in option tag
      
         $data["stores"] = $this->{$this->model}->all(5, "store_code", "DESC",array("deleted"=>"False"))->result_object();
        $this->admin_template("index", $data, "script");
    }

    public function sort()
    {
        $this->load->model("store_type_model");
        $data["store_type"] = $this->store_type_model->all()->result_object(); //load store type in option tag
        $this->load->library('form_validation');
        $this->form_validation->set_rules('keyword', 'Keywords', 'required');
        if($this->input->post()) 
        {

            if ($this->form_validation->run() === FALSE) 
            {
                $this->session->set_flashdata('error', validation_errors());
                redirect(base_url()."store");
            }else
            {
                $data["search"] = 'Search Result for "'.$this->input->post('keyword').'"';
                $data["stores"] = $this->{$this->model}->get_like(array($this->input->post('search')=>$this->input->post('keyword')),array("deleted" => "False"))->result_object();
            }
        
        }
     
        $this->admin_template("index", $data, "script");
    }

    public function edit($store_code = FALSE)
    {
        if($store_code === FALSE)
            redirect(base_url().$this->module);
        $this->load->model("store_type_model");
        $data["store_type"] = $this->store_type_model->all()->result_object();
        $data["value"] = $this->{$this->model}->get(array("store_code"=>$store_code))->result_object();

        $this->admin_template("edit", $data, "script");
    }

    public function save()
    {
        if($_SERVER["REQUEST_METHOD"] === "GET") {
            $this->session->set_flashdata('error', "Invalid Request Method!");
            redirect(base_url().$this->module);
            exit();
        }
        //add validation here
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name_code', 'Name Code', 'required|numeric');
        $this->form_validation->set_rules('store_name', 'Store Name', 'required');
        $this->form_validation->set_rules('pc_code', 'PC Code', 'required|numeric');
        $this->form_validation->set_rules('store_type', 'Store Type', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error', validation_errors());
        } else {
            $count = $this->{$this->model}->get_last_store()->result_object()[0];
            $count = str_replace("S", "", $count->store_code) + 1;
            $store_code = "S" . str_pad($count,5,"0",STR_PAD_LEFT);
            $data = array(
                    "store_code"    =>  $store_code,
                    "name_code"     =>  $this->input->post("name_code"),
                    "store_name"     =>  $this->input->post("store_name"),
                    "pc_code"       =>  "PC#".$this->input->post("pc_code"),
                    "store_type"    =>  $this->input->post("store_type"),
                    "deleted"       =>  "False",
                    "mic_code"      =>  null
                );
            $result = $this->{$this->model}->add($data);
            if($result) {
                $this->session->set_flashdata('success', "Store Successfully Saved!");
            } else {
                $this->session->set_flashdata('error', "Unable to save to database!");
            }
        }
        redirect(base_url().$this->module);
    }

    public function update()
    {
        if($_SERVER["REQUEST_METHOD"] === "GET") {
            $this->session->set_flashdata('error', "Invalid Request Method!");
            redirect(base_url().$this->module);
            exit();
        }
        //add validation here
        $this->load->library('form_validation');
        $this->form_validation->set_rules('store_code', 'Store Code', 'required');
        $this->form_validation->set_rules('name_code', 'Name Code', 'required|numeric');
        $this->form_validation->set_rules('store_name', 'Store Name', 'required');
        $this->form_validation->set_rules('pc_code', 'PC Code', 'required|numeric');
        $this->form_validation->set_rules('store_type', 'Store Type', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error', validation_errors());
        } else {
            $where  = array(
                    "store_code"    =>  $this->input->post("store_code")
                );
            $update = array(
                    "name_code"     =>  $this->input->post("name_code"),
                    "store_name"    =>  $this->input->post("store_name"),
                    "pc_code"       =>  "PC#".$this->input->post("pc_code"),
                    "store_type"    =>  $this->input->post("store_type")
                );
            $result = $this->{$this->model}->edit($where, $update);
            if($result) {
                $this->session->set_flashdata('success', "Store Successfully Updated!");
            } else {
                $this->session->set_flashdata('error', "Unable to save to database!");
            }
        }
        redirect(base_url().$this->module);
    }
    public function delete($storeid)
    {
          $where = array("store_code" => $storeid);
          $update = array("deleted" => "True");
          //$this->load->model("store_type_model");
          $result = $this->{$this->model}->edit($where,$update);
          //redirect(base_url().$this->module);
           echo json_encode($result);

    }

}

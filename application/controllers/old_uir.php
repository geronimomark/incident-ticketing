<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Old_uir extends MY_Controller {

	public $model = "reports_model";
	public $module = "old_uir";
	public $method = "search";
	public $limit = 10;

	public function __construct()
	{
		parent::__construct();
		$this->title  = "REPORTS";
		$this->active = "reports";
		if(! Sess::check_session()) {
			redirect(base_url()."login");
		}
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
  <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email extends MY_Controller {

	public $model = "uir_new_model";
	public $module = "email";
	public $method = "search";
	public $limit = 10;

	public function __construct()
	{
		parent::__construct();
		$this->title  = "EMAIL";
		$this->active = "reports";
		if($_POST && ! Sess::check_session()) {
			echo json_encode(array("error"=>"Session Expired"));
			exit();
		}
		if(! Sess::check_session()) {
			redirect(base_url()."login");
		}
		$this->load->model($this->model);
	}

	public function index()
	{
		if($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('search', 'Search By', 'required');
			$this->form_validation->set_rules('keyword', 'Keywords', 'required');
			if ($this->form_validation->run() === FALSE) {
				$this->session->set_flashdata('error', validation_errors());
				redirect(base_url()."email");
			} else {
				$view["search"] = "Result by searching UIR like '" . $this->input->post("keyword") . "'";
				$data = array("field"=>$this->input->post("search"), "search"=>$this->input->post("keyword"),"remove"=>"0");
			}
		} else {
			$view["search"] = "Today's UIR";
			$data = FALSE;
		}
		$view["data"] = $this->{$this->model}->get_uir($data)->result_object();
		$this->admin_template("index", $view, "script");
	}

	public function view($ticket)
	{
		echo json_encode($this->{$this->model}->get_view(array("A.uir_ticket_no"=>$ticket))->result_object());
	}

	public function resend($ticket)
	{
		$data = $this->{$this->model}->get_view(array("A.uir_ticket_no"=>$ticket))->result_object();
		foreach ($data as $key => $value) {
			$data2[] = array(
						"uir_ticket_no"		=>	$value->uir_ticket_no,
						"label"				=>	$value->label,
						"value"				=>	$value->value
					);
		}
		$email_data = array(
				"pc"			=>	$data[0]->pc_code,
				"seccode"		=>	$data[0]->security_code,
				"incident"		=>	$data[0]->incident_name,
				"uir_code"		=>	$data[0]->uir_code,
				"ticket"		=>	$data[0]->uir_ticket_no,
				"uir_class"		=>	$data[0]->main_name,
				"store_no"		=>	$data[0]->name_code,
				"store_name"	=>	$data[0]->store_name,
				"uir_date"		=>	$data[0]->uir_date,
				"uir_time"		=>	$data[0]->uir_time,
				"mic"			=>	$data[0]->mic_name,
				"sg"			=>	$data[0]->sg_name,
				"summary"		=>	$data[0]->summary,
				"location"		=>	$data[0]->location,
				"initial_action"=>	$data[0]->initial_action,
				"others"		=>	$data2
			);
		$primary = get_recipient($data[0]->security_code,1,$data[0]->recipient);
		$secondary = get_recipient($data[0]->security_code,2,0);
		$message = generate_html_mail($email_data);
		$email_result = email($message, $primary, $secondary, $data[0]->store_name."-".$data[0]->incident_name);
		if($email_result) {
			echo json_encode(array("message"=>"Email is resent!"));
		} else {
			echo json_encode(array("message"=>"Unable to send email!"));
		}
	}
	public function delete_uir($ticket){
		$data = array("remove"		=>	"1",);
		$this->load->model("uir_new_model");
		$result = $this->uir_new_model->edit(array("uir_ticket_no"=>$ticket),$data);
		if($result){
		redirect(base_url()."email");
		}
	}
}
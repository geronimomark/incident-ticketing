<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Extract extends MY_Controller {

	public $model = "uir_new_model";
	public $module = "extract";
	public $method = "";
	public $limit = 10;

	public function __construct()
	{
		parent::__construct();
		$this->title  = "EXTRACT";
		$this->active = "reports";
		if($_POST && ! Sess::check_session()) {
			echo json_encode(array("error"=>"Session Expired"));
			exit();
		}
		if(! Sess::check_session()) {
			redirect(base_url()."login");
		}
		$this->load->model($this->model);
	}

	public function index()
	{
		$this->load->model("vtype_model");
        $data["uir_class"] = $this->vtype_model->get(array("deleted"=>"False"))->result_object();
        $this->admin_template("index", $data, "script");
	}
    public function sample(){
        $a['hey'] = 'abc';
        $a['oi'] = '246';
        echo $a['oi'];
    }
	public function generate_excel()
	{
		$extension_title ="";
        $extension_date;
        $where_join;
        $where_distinct;
        if($this->input->post('date')){
            
            $where_join['A.uir_date'] = $this->input->post('date');
            $where_distinct['B.uir_date'] = $this->input->post('date');
            //$where_join = array("A.uir_date"=>$this->input->post('date'));
            //$where_distinct = array("B.uir_date"=>$this->input->post('date'));
            $extension_date = str_replace("-","",$this->input->post('date'));

        }else{
            $year = $this->input->post("year");
            $month = $this->input->post("month");  
            $date = $year . "-" . $month;
           // $end = date("Y-m-t", strtotime($start)); //modified by JBT enhancement
            $where_join = array("DATE_FORMAT(A.uir_date,'%Y-%m')"=>$date);
            $where_distinct = array("DATE_FORMAT(B.uir_date,'%Y-%m')"=>$date);
            $extension_date = $date;
        }
        if($this->input->post('uir_class'))
        {
            $where_join['C.main_code'] = $this->input->post('uir_class');
            $where_distinct['D.main_code'] = $this->input->post('uir_class');
            $extension_title = $this->input->post('uir_class');
        }
        if($this->input->post('incident'))
        {
            $where_join['A.incident_code'] = $this->input->post('incident');
            $where_distinct['B.incident_code'] = $this->input->post('incident');
            $extension_title = strtoupper($this->input->post('incident'));
        }
        if($this->input->post('store'))
        {
            $store  = explode("-", $this->input->post("store")); //explode the store format S00001-STORENAME

            $where_join['A.store_name_code'] = $store[0];
            $where_distinct['B.store_name_code'] = $store[0];
            $extension_title = strtoupper($store[1]);
        }

      

		//$start = $year . "-" . $month . "-01";
		

		$data = $this->{$this->model}->get_view($where_join);
     
      
		//load our new PHPExcel library
        $this->load->library('excel');

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);

        //name the worksheet
        $this->excel->getActiveSheet()->setTitle("GRANDEUR-UIR-".$extension_title);

        $borderStyle = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000'),
                    ),
                )
            );

        $border_top = array(
            'borders' => array(
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array('rgb' => '000000'),
                    ),
                ),
            );

        $border_color = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => 'C0C0C0',
                    ),
            )
        );

        $font_title = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => 'FFFFFF'),
                'name'  => 'Arial'
            )); 

        $border_color_inactive = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => 'FF0000',
                    ),
            )
        );

        $border_color_title = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => '000000',
                    ),
            )
        );

        //set cell A1 content with some text
        // $this->excel->getActiveSheet()->setCellValue('A2', 'Supervisor :');

        // $this->excel->getActiveSheet()->setCellValue('A3', 'Title :');

        // $this->excel->getActiveSheet()->setCellValue('A4', 'As of :');

        // content
        // $this->excel->getActiveSheet()->setCellValue('B2', 'ODSI');

        // $this->excel->getActiveSheet()->setCellValue('B3', 'STORE LIST');

        // $this->excel->getActiveSheet()->setCellValue('B4', $month.$year);

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(100);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(100);
        
        //get list of dynamic labels
        $this->load->model("uir_details_model");
        $data_label_count = $this->uir_details_model->get_distinct("count(distinct(A.label)) as label_count",$where_distinct);
        $row = $data_label_count->row();
        //satrt her dynamic column


         //   $i = $i+9+$data_label_count->label;//index +8 to start in collumn I
          $x = $row->label_count;
          for ($i=11; $i <= $x+10 ; $i++) { 
            $this->excel->getActiveSheet()->getColumnDimension($this->collumn_array($i))->setWidth(40);
	    $this->excel->getActiveSheet()->getStyle($this->collumn_array($i))->getAlignment()->setWrapText(true);
          }

        //$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(40);
        //$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(40);
        //$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(40);
        //$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(40);
        //$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(40);
        //$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(40);
        //$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(40);

        //wrap column
        $this->excel->getActiveSheet()->getStyle('G')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('J')->getAlignment()->setWrapText(true);

        //format to number w/out decimal
        // $this->excel->getActiveSheet()->getStyle('C')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

        //legend
        $this->excel->getActiveSheet()->setCellValue('D2', 'GRANDEUR SECURITY AND SERVICES CORPORATION');
        $this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($border_color_title);
        $this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($font_title);

        $this->excel->getActiveSheet()->setCellValue('D3', 'Summary Report');

        $this->excel->getActiveSheet()->setCellValue('D4', "GRANDEUR-UIR-".$extension_title);
        $this->excel->getActiveSheet()->getStyle('D4')->applyFromArray($border_color_inactive);

        //table headers
        $this->excel->getActiveSheet()->setCellValue('A7', '#');
        $this->excel->getActiveSheet()->getStyle('A7')->applyFromArray($border_color_title);
        $this->excel->getActiveSheet()->getStyle('A7')->applyFromArray($font_title);
        
        //added new fields
        $this->excel->getActiveSheet()->setCellValue('B7', 'Ticket No.');
        $this->excel->getActiveSheet()->getStyle('B7')->applyFromArray($border_color_title);
        $this->excel->getActiveSheet()->getStyle('B7')->applyFromArray($font_title);

        $this->excel->getActiveSheet()->setCellValue('C7', 'PC No.');
        $this->excel->getActiveSheet()->getStyle('C7')->applyFromArray($border_color_title);
        $this->excel->getActiveSheet()->getStyle('C7')->applyFromArray($font_title);
        //added new fields

        $this->excel->getActiveSheet()->setCellValue('D7', 'Store');
        $this->excel->getActiveSheet()->getStyle('D7')->applyFromArray($border_color_title);
        $this->excel->getActiveSheet()->getStyle('D7')->applyFromArray($font_title);

        

        $this->excel->getActiveSheet()->setCellValue('E7', 'Date');
        $this->excel->getActiveSheet()->getStyle('E7')->applyFromArray($border_color_title);
        $this->excel->getActiveSheet()->getStyle('E7')->applyFromArray($font_title);

        $this->excel->getActiveSheet()->setCellValue('F7', 'Time');
        $this->excel->getActiveSheet()->getStyle('F7')->applyFromArray($border_color_title);
        $this->excel->getActiveSheet()->getStyle('F7')->applyFromArray($font_title);

        $this->excel->getActiveSheet()->setCellValue('G7', 'Summary');
        $this->excel->getActiveSheet()->getStyle('G7')->applyFromArray($border_color_title);
        $this->excel->getActiveSheet()->getStyle('G7')->applyFromArray($font_title);

       // $this->excel->getActiveSheet()->setCellValue('F7', 'Employee2');
       // $this->excel->getActiveSheet()->getStyle('F7')->applyFromArray($border_color_title);
       // $this->excel->getActiveSheet()->getStyle('F7')->applyFromArray($font_title);

        $this->excel->getActiveSheet()->setCellValue('H7', 'MIC');
        $this->excel->getActiveSheet()->getStyle('H7')->applyFromArray($border_color_title);
        $this->excel->getActiveSheet()->getStyle('H7')->applyFromArray($font_title);

        $this->excel->getActiveSheet()->setCellValue('I7', 'SG');
        $this->excel->getActiveSheet()->getStyle('I7')->applyFromArray($border_color_title);
        $this->excel->getActiveSheet()->getStyle('I7')->applyFromArray($font_title);

        $this->excel->getActiveSheet()->setCellValue('J7', 'Action Taken');
        $this->excel->getActiveSheet()->getStyle('J7')->applyFromArray($border_color_title);
        $this->excel->getActiveSheet()->getStyle('J7')->applyFromArray($font_title);

        //start here dynamic column
        $this->load->model("uir_details_model");
        $data_label = $this->uir_details_model->get_distinct("distinct(A.label) as label",$where_distinct);
        $i=11;
        foreach ($data_label->result() as $value)
        {
            
            $this->excel->getActiveSheet()->setCellValue($this->collumn_array($i).'7', $value->label);
            $this->excel->getActiveSheet()->getStyle($this->collumn_array($i).'7')->applyFromArray($border_color_title);
            $this->excel->getActiveSheet()->getStyle($this->collumn_array($i).'7')->applyFromArray($font_title);   
            $label_list[$value->label] = $this->collumn_array($i);//add to array label to find the cell dimention when inserting values
        $i++;
        }
        //end here
       
        $ctr = 8;
        $num = 1;
        $subctr = 0;
        $main = "";
        $sub = "";
        $ticket = "";

        foreach ($data->result_object() as $key => $value){

        	if($main != $value->main_name) {
        		$ctr++;
        		$main = $value->main_name;
        		$this->excel->getActiveSheet()->setCellValue('B'.$ctr,$main);
        		$this->excel->getActiveSheet()->getStyle('B'.$ctr)->getFont()->setBold(true);
        		$ctr++;
        	}

        	if($sub != $value->incident_name) {
				$sub = $value->incident_name;
        		$this->excel->getActiveSheet()->setCellValue('B'.$ctr,$sub);
        		$this->excel->getActiveSheet()->getStyle('B'.$ctr)->getFont()->setBold(true);
        		$ctr++;
        	}

        	if($ticket != $value->uir_ticket_no) {
        		$subctr = 0;
        		$ticket = $value->uir_ticket_no;
        		$this->excel->getActiveSheet()->setCellValue('A'.$ctr,$num);
	            //added fields
                $this->excel->getActiveSheet()->setCellValue("B".$ctr,"'".$value->uir_ticket_no);
                $this->excel->getActiveSheet()->setCellValue('C'.$ctr,$value->pc_code);

                $this->excel->getActiveSheet()->setCellValue('D'.$ctr,$value->store_name);
	            $this->excel->getActiveSheet()->setCellValue('E'.$ctr,$value->uir_date);
	            $this->excel->getActiveSheet()->setCellValue('F'.$ctr,$value->uir_time);
	            $this->excel->getActiveSheet()->setCellValue('G'.$ctr,$value->summary);
	            //$this->excel->getActiveSheet()->setCellValue('F'.$ctr,$value->store_name);
	            $this->excel->getActiveSheet()->setCellValue('H'.$ctr,$value->mic_name);
	            $this->excel->getActiveSheet()->setCellValue('I'.$ctr,$value->sg_name);
	            $this->excel->getActiveSheet()->setCellValue('J'.$ctr,$value->initial_action);
	            //$this->excel->getActiveSheet()->setCellValue('I'.$ctr,$value->label . "-" .$value->value);

	            $this->excel->getActiveSheet()->getStyle('A'.$ctr)->applyFromArray($borderStyle);
	            $this->excel->getActiveSheet()->getStyle('B'.$ctr)->applyFromArray($borderStyle);
	            $this->excel->getActiveSheet()->getStyle('C'.$ctr)->applyFromArray($borderStyle);
	            $this->excel->getActiveSheet()->getStyle('D'.$ctr)->applyFromArray($borderStyle);
	            $this->excel->getActiveSheet()->getStyle('E'.$ctr)->applyFromArray($borderStyle);
	           // $this->excel->getActiveSheet()->getStyle('F'.$ctr)->applyFromArray($borderStyle);
	            $this->excel->getActiveSheet()->getStyle('F'.$ctr)->applyFromArray($borderStyle);
	            $this->excel->getActiveSheet()->getStyle('G'.$ctr)->applyFromArray($borderStyle);
	            $this->excel->getActiveSheet()->getStyle('H'.$ctr)->applyFromArray($borderStyle);
                $this->excel->getActiveSheet()->getStyle('I'.$ctr)->applyFromArray($borderStyle);
                $this->excel->getActiveSheet()->getStyle('J'.$ctr)->applyFromArray($borderStyle);
	           //$this->excel->getActiveSheet()->getStyle('I'.$ctr)->applyFromArray($borderStyle);
        	}
            if($ticket == $value->uir_ticket_no) {
                if($subctr != 0) {
                    $ctr--;
                    $num--;
                }
                 $subctr++;
                 if(isset($label_list[$value->label])) {
                     $this->excel->getActiveSheet()->setCellValue($label_list[$value->label]."".$ctr,$value->value);
                     $this->excel->getActiveSheet()->getStyle($label_list[$value->label]."".$ctr)->applyFromArray($borderStyle);
                 }
            }

            $ctr++;
            $num++;
        }

        //setting font to bold
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);

        $this->excel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('D3')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('D4')->getFont()->setBold(true);


        //setting border color
        //$this->excel->getActiveSheet()->getStyle('A16:E16')->applyFromArray($border_color);
        //$this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($border_color);

        //setting borders for merge cells
        //$this->excel->getActiveSheet()->getStyle('A5:E5')->applyFromArray($borderStyle);
        

        //setting borders for single cell
        //$this->excel->getActiveSheet()->getStyle('E6')->applyFromArray($borderStyle);

        $this->excel->getActiveSheet()->mergeCells('D2:F2'); //put it on the loop
        $this->excel->getActiveSheet()->mergeCells('D3:F3'); 
        $this->excel->getActiveSheet()->mergeCells('D4:F4');

        $this->excel->getActiveSheet()->getStyle('D2:F2')->applyFromArray($borderStyle);
        $this->excel->getActiveSheet()->getStyle('D3:F3')->applyFromArray($borderStyle);
        $this->excel->getActiveSheet()->getStyle('D4:F4')->applyFromArray($borderStyle);
 

        //set aligment
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $this->excel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $filename="GRANDEUR-UIR-".$extension_date.'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        redirect(base_url().$this->module);

	}
    public function collumn_array($index)
    {
     $item = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
        $cntr=0;
        $index = $index-1;
        if ($index > 25)
        {
          while ($index > 25) {
            $cntr++;
            $index = $index - 26;
          }
     }
     if ($cntr != 0)
     {
        $data = $item[$cntr-1]."".$item[$index];
     }else
     {
        $data = $item[$index];
     }
   return $data;
    }
   

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Uir_update extends MY_Controller {

	public $model = "uir_new_model";
	public $module = "uir_update";
	public $method = "search";
	public $limit = 10;


	public function __construct()
	{
		parent::__construct();
		$this->title  = "UIR UPDATE";
		$this->active = "uir_update";
		if(strpos($this->uri->segment(2), "get") !== FALSE && ! Sess::check_session()) {
			echo json_encode(array("error"=>"Session Expired"));
			exit();
		}
		if(! Sess::check_session()) {
			redirect(base_url()."login");
		}

	}
public function index()
	{
		redirect(base_url()."uir_update/id/13");
				
	}

	public function id($id)
	{
		

		$this->load->model("vtype_model");
		$data["uir_class"] = $this->vtype_model->get(array("deleted"=>"False"))->result_object();
		$this->admin_template_UIR("index", $data, "script","UPDATE");
	}
}

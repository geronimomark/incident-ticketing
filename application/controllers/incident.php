<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Incident extends MY_Controller {

    public $model = "store_model";
    public $module = "incident";
    public $method = "";
    public $limit = 10;

    public function __construct()
    {
        parent::__construct();
        $this->title  = "INCIDENT INFORMATION";
        $this->active = "maintenance";
        if($_POST && ! Sess::check_session()) {
            echo json_encode(array("error"=>"Session Expired"));
            exit();
        }
        if(! Sess::check_session()) {
            redirect(base_url()."login");
        }
        $this->load->model($this->model);
    }

    public function index()
    {
        
        $this->load->model("vtype_model");
        $data["class_name"] = $this->vtype_model->all(false,'main_name','ASC',array('deleted'=>'false'))->result_object(); //load store type in option tag
      
       //  $data["stores"] = $this->{$this->model}->all(5, "store_code", "DESC",array("deleted"=>"False"))->result_object();
        $this->admin_template("index", $data, "script");
    }
      public function sort()
    {
        
        $this->load->model("vtype_model");
        $data["class_name"] = $this->vtype_model->all(false,'main_name','ASC',array('deleted'=>'false'))->result_object(); //load store type in option tag
        $this->load->model("incident_model");
        $data["incident"] = $this->incident_model->all(false,'incident_name','ASC',array('vtype_main_code' => $this->input->post('class_name'),'status'=>'0'))->result_object(); //load store type in option tag     
       //  $data["stores"] = $this->{$this->model}->all(5, "store_code", "DESC",array("deleted"=>"False"))->result_object();
        $this->admin_template("index", $data, "script");
    }
    public function delete_incident($code){
        $this->load->model("incident_model");
        $fields  = array("status" => "1");
        $result = $this->incident_model->edit(array("incident_code"=>$code),$fields);
        echo json_encode($result);
    }
    public function incident_info($incident_code)
    {
        $this->load->model("incident_model");
        $data["incidents"] = $this->incident_model->get_with_class(array('A.incident_code' => $incident_code))->result_object(); //load store type in option tag
        $data["sec_code"] = $this->incident_model->distinct_field('security_code',array('status' => '0'))->result_object();
        $this->load->model("vtype_model");
        $data["vtype"] = $this->vtype_model->get(array('deleted'=>'false'))->result_object();
        
        $details = $this->load->view($this->module."/incident_details", $data,true);
        //$details = $this->load->view($this->module."/physical_injuries",array('val'=>strtolower($search)), true);
        echo json_encode(array("data"=>$details));
    }
    public function update_incident2($data)
    {
        $str = explode(":", $data);
        $where = array('incident_code' => $str[0]);
        $fields = array('incident_name' => $str[1],'vtype_main_code' => $str[2], 'security_code' => $str[3]);
        $this->load->model("incident_model");

        $result = $this->incident_model->edit($where,$fields);
        echo json_encode($result);
    }
        public function update_incident($incident_code)
    {
        if($this->input->post()) {
        $where = array('incident_code' => $incident_code);
        $fields = array('incident_name' => $this->input->post('incident_name_update'),'vtype_main_code' => $this->input->post('class_name'), 'security_code' => $this->input->post('sec_code'));
        $this->load->model("incident_model");

        $result = $this->incident_model->edit($where,$fields);
            if($result){
                 redirect(base_url()."incident/sort");
            }else
            {
                echo "error";
            }
        }
        else
        {
             redirect(base_url()."incident/sort");
        }
    }

   
}

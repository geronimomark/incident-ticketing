<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view("template/admin/login");
    }

    public function authenticate()
    {
        if($_SERVER["REQUEST_METHOD"] === "POST") {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata("error", validation_errors());
            } else {
                $this->load->model("accounts_model");
                $where = array(
                        "user_username" =>  $this->input->post("username"),
                        "user_password" =>  $this->input->post("password")
                    );
                $result = $this->accounts_model->get($where);
                if($result->num_rows === 1) {
                    $this->set_session($result->result_object()[0]);
                    redirect(base_url()."home");
                } else {
                    $this->session->set_flashdata("error", "Invalid Username / Password!");
                }
            }
        } else {
            $this->session->set_flashdata("error", "You cannot access this module.");
        }
        redirect(base_url()."login");
    }

    private function set_session($data)
    {
        $session = array(
                "user_code" => $data->user_code,
                "name"      => $data->user_name,
                "username"  => $data->user_username,
                "password"  => $data->user_password,
            );
        $this->session->set_userdata($session);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url()."login");
    }
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
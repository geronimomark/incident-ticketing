<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Uir_new extends MY_Controller {

	public $model = "uir_new_model";
	public $module = "uir_new";
	public $method = "search";
	public $limit = 10;

	public function __construct()
	{
		parent::__construct();
		$this->title  = "UIR";
		$this->active = "uir_new";
		if(strpos($this->uri->segment(2), "get") !== FALSE && ! Sess::check_session()) {
			echo json_encode(array("error"=>"Session Expired"));
			exit();
		}
		if(! Sess::check_session()) {
			redirect(base_url()."login");
		}
	}

	public function index()
	{
		$this->load->model("vtype_model");
		$data["uir_class"] = $this->vtype_model->get(array("deleted"=>"False"))->result_object();
		$this->admin_template_UIR("index", $data, "script","SAVE");
	}

	public function get_sub($vtype)
	{
		$this->load->model("incident_model");
		$data = $this->incident_model->get(array("vtype_main_code"=>$vtype,"`status`"=>"0"))->result_object();
		echo json_encode($data);
	}

	public function get_mic($search, $store)
	{
		$store = explode("-", $store);
		$this->load->model("mic_model");
		$data = $this->mic_model->get("store_name = '{$store[0]}' AND deleted = 'FALSE' AND (mic_name LIKE '%{$search}%' OR mic_code LIKE '%{$search}%') LIMIT 10")->result_object();
		echo json_encode($data);
	}

	public function get_sg($search, $store)
	{
		$store = explode("-", $store);
		$this->load->model("sg_model");
		$data = $this->sg_model->get("store_code = '{$store[0]}' AND deleted = 'FALSE' AND (sg_name LIKE '%{$search}%' OR sg_code LIKE '%{$search}%') LIMIT 10")->result_object();
		echo json_encode($data);
	}

	public function get_store($search)
	{
		$this->load->model("store_model");
		$data = $this->store_model->get("deleted = 'FALSE' AND (store_name LIKE '%{$search}%' OR store_code LIKE '%{$search}%') LIMIT 10")->result_object();
		echo json_encode($data);
	}

	public function get_employee($search, $store)
	{
		$store = explode("-", $store);
		$this->load->model("employee_model");
		$data = $this->employee_model->get("store_code = '{$store[0]}' AND deleted = 'FALSE' AND (emp_code LIKE '%{$search}%' OR emp_fname LIKE '%{$search}%' OR emp_sname LIKE '%{$search}%') LIMIT 10")->result_object();
		echo json_encode($data);
	}
	public function get_details($search)
	{
		if($search === "MT") {
			$details = $this->load->view($this->module."/managers_theft", '', true);
		
		} elseif ($search === "SAF" OR $search === "B" OR $search === "OA" OR $search === "VA") {
			$details = $this->load->view($this->module."/SAF_B_OA_VA",array('val'=>strtolower($search)), true);
		
		} elseif ($search === "USA" OR $search === "CFV" OR $search === "SV" OR $search === "L-O" OR $search === "BIV" OR $search === "BPV" OR $search === "VKY-VKEYS") {
			$details = $this->load->view($this->module."/USA_CFV_SV_LO_BIV_BPV_VKY",array('val'=>strtolower($search)), true);
		
		} elseif ($search === "CS" OR $search === "CO" OR $search === "CCV" OR $search === "FBV") {
			$details = $this->load->view($this->module."/CS_CO_CCV_FBV",array('val'=>strtolower($search)), true);
		
		} elseif ($search === "PUTD-UTD" OR $search === "8MW" OR $search === "UW" OR $search === "W" OR $search === "D8C") {
			$details = $this->load->view($this->module."/PUTD_8MW_UW_W_D8C",array('val'=>strtolower($search)), true);
		
		} elseif ($search === "ALM-EAS" OR $search === "CCTV-CTV" OR $search === "DE-DEE" OR $search === "L") {
			$details = $this->load->view($this->module."/EAS_CCTV_DE_L",array('val'=>strtolower($search)), true);
		
		}elseif ($search === "TSC" OR $search === "BPI" OR $search === "UCO" OR $search === "GDC" OR $search === "HD" OR $search === "DTP" OR $search === "LAK") {
			$details = $this->load->view($this->module."/TSC_BPI_UCO_GDC_HD_DTP_LAK",array('val'=>strtolower($search)), true);
		
		} elseif ($search === "TDP") {
			$details = $this->load->view($this->module."/traffic_drive_thru_parking", '', true);
		
		} elseif ($search === "F") {
			$details = $this->load->view($this->module."/fire", '', true);
		
		} elseif ($search === "NIR" OR $search === "VMS" OR $search === "PT") {
			$details = $this->load->view($this->module."/NIR_VMS",array('val'=>strtolower($search)), true);
		
		} elseif ($search === "UV") {
			$details = $this->load->view($this->module."/uniform_violation", '', true);
		
		} elseif ($search === "NIT" OR $search === "NIT-NITT") {
			$details = $this->load->view($this->module."/none_issuance_transfer_ticket", '', true);
		
		} elseif ($search === "LAF") {
			$details = $this->load->view($this->module."/loss_and_found", '', true);
		
		} elseif ($search === "RA") {
			$details = $this->load->view($this->module."/rider_accident", '', true);
		
		} elseif ($search === "EOD") {
			$details = $this->load->view($this->module."/eating_on_duty", '', true);
		
		} elseif ($search === "D8C") {
			$details = $this->load->view($this->module."/dial_8_case", '', true);
		
		} elseif ($search === "8MW") {
			$details = $this->load->view($this->module."/8mcdo_waste", '', true);
		
		} elseif ($search === "UW") {
			$details = $this->load->view($this->module."/8mcdo_waste", '', true);
		
		} elseif ($search === "W") {
			$details = $this->load->view($this->module."/waste", '', true);
		
		} elseif ($search === "EMV") {
			$details = $this->load->view($this->module."/employee_meal_violation", '', true);
		
		} elseif ($search === "FSC-FSCC") {
			$details = $this->load->view($this->module."/food_service_customer_complaint", '', true);
		
		} elseif ($search === "T") {
			$details = $this->load->view($this->module."/tardiness", '', true);
		
		} elseif ($search === "LFO") {
			$details = $this->load->view($this->module."/party_food_lfo", '', true);
		
		} elseif ($search === "PIL") {
			$details = $this->load->view($this->module."/pilferage", '', true);
		
		}elseif ($search === "FE") {
			$details = $this->load->view($this->module."/fire_extinguisher", '', true);
		
		}elseif ($search === "FBD") {
			$details = $this->load->view($this->module."/defective_fake_bill_detector", '', true);
		
		}elseif ($search === "ET") {
			$details = $this->load->view($this->module."/employees_theft", '', true);
		
		}elseif ($search === "SC" OR $search === "BB" OR $search === "BK") {
			$details = $this->load->view($this->module."/SC_BB_BK",array('val'=>strtolower($search)), true);
		
		}elseif ($search === "DAI-D-AI") {
			$details = $this->load->view($this->module."/drugs_alcohol", '', true);
		
		}elseif ($search === "H" OR $search === "TRT") {
			$details = $this->load->view($this->module."/H_TRT",array('val'=>strtolower($search)), true);
		
		}elseif ($search === "CTF") {
			$details = $this->load->view($this->module."/challenging_to_fight",array('val'=>strtolower($search)), true);
		
		}elseif ($search === "R") {
			$details = $this->load->view($this->module."/robbery",array('val'=>strtolower($search)), true);
		
		}elseif ($search === "BT") {
			$details = $this->load->view($this->module."/bomb_threat",array('val'=>strtolower($search)), true);
		
		}elseif ($search === "C") {
			$details = $this->load->view($this->module."/carnapping",array('val'=>strtolower($search)), true);
		
		}elseif ($search === "EO") {
			$details = $this->load->view($this->module."/entrapment_operations",array('val'=>strtolower($search)), true);
		
		}elseif ($search === "PDW-IPDW") {
			$details = $this->load->view($this->module."/possession_of_deadly_weapon",array('val'=>strtolower($search)), true);
		
		}elseif ($search === "OI" OR $search === "PI" OR $search === "MSC" OR $search === "EIP") {
			$details = $this->load->view($this->module."/other_information",array('val'=>strtolower($search)), true);
		
		}elseif ($search === "PI") {
			$details = $this->load->view($this->module."/physical_injuries",array('val'=>strtolower($search)), true);
		
		}else { 
			$details = "";
		}

		echo json_encode(array("data"=>$details));
	}

	public function get_otherfield($ticket)
	{
	//	$this->vtype_model->get(array("deleted"=>"False"))->result_object();
		$this->load->model("uir_new_model");
		echo json_encode($this->uir_new_model->get_uir_otherfield(array("A.uir_ticket_no"=>$ticket))->result_object());
		
	}
	// public function get_details($search)
	// {
	// 	if($search === "MT") {
	// 		$details = $this->load->view($this->module."/managers_theft", '', true);
	// 	} else {
	// 		$details = "";
	// 	}
		
	// 	echo json_encode(array("data"=>$details));
	// }

	public function save()
	{
		if($_SERVER["REQUEST_METHOD"] === "GET") {
			$this->session->set_flashdata('errorx', "Invalid Request Method!");
			redirect(base_url().$this->module);
			exit();
		}
		//add validation here
		$this->load->library('form_validation');
		$this->form_validation->set_rules('uir_code_value', 'UIR Code', 'required');
		$this->form_validation->set_rules('incident', 'Incident', 'required');
		$this->form_validation->set_rules('date', 'Date of Discovery', 'required');
		$this->form_validation->set_rules('time', 'Time of Discovery', 'required');
		$this->form_validation->set_rules('store', 'Store', 'required');
		$this->form_validation->set_rules('rm_mic', 'RM/MIC', 'required');
		$this->form_validation->set_rules('sg', 'SG', 'required');
		$this->form_validation->set_rules('location', 'Incident Location', 'required');
		$this->form_validation->set_rules('act', 'Initial Action', 'required');
		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('errorx', validation_errors());
		} else {
		//end validation
			$store 	= explode("-", $this->input->post("store"));
			$pc 	= explode("#", $this->input->post("store"));
			$mic 	= explode("-", $this->input->post("rm_mic"));
			$sg 	= explode("-", $this->input->post("sg"));
			$uir  	= strtolower($this->input->post("uir_code_value"));
			$ticket = $this->generate_ticket_no();

			$this->load->model("uir_new_model");
			$this->load->model("uir_details_model");

			$data = array(
					"uir_ticket_no"		=>	$ticket,
					"incident_code"		=>	$this->input->post("incident"),
					"store_name_code"	=>	$store[0],
					"uir_date"			=>	$this->input->post("date"),
					"uir_time"			=>	$this->input->post("time"),
					"mic_code"			=>	$mic[0],
					"sg_code"			=>	$sg[0],
					"summary"			=>	$this->input->post("summary"),
					"location"			=>	$this->input->post("location"),
					"initial_action"	=>	$this->input->post("act"),
					"last_update"		=>	date("Y-m-d H:i:s"),
					"recipient"			=>	$this->input->post("email")
				);

			$ctr = 0;
			$data2 = array();
			foreach ($this->input->post() as $key => $value) {
				if(strpos($key, "label") !== FALSE) {
					$ctr++;
					$data2[] = array(
							"uir_ticket_no"		=>	$ticket,
							"label"				=>	$this->input->post("label_".$uir.$ctr),
							"value"				=>	$this->input->post("value_".$uir.$ctr)
						);
					$this->form_validation->set_rules("label_".$uir.$ctr, $this->input->post("label_".$uir.$ctr) . "Label", 'required');
					$this->form_validation->set_rules("value_".$uir.$ctr, $this->input->post("label_".$uir.$ctr), 'required');
				}
			}
			if ($this->form_validation->run() === FALSE) {
				$this->session->set_flashdata('errorx', validation_errors());
			} else {
				$result = $this->uir_new_model->add($data);
				if($result === TRUE) {
					$result = $this->uir_details_model->add_batch($data2);
					if($result) {
						$this->load->model("incident_model");
						$incident_name = $this->incident_model->get_with_class(array("incident_code"=>$this->input->post("incident")))->result_object()[0];
						$email_data = array(
								"pc"			=>	$pc[1],
								"seccode"		=>	strtolower($this->input->post("sec_code_value")),
								"incident"		=>	$incident_name->incident_name,
								"uir_code"		=>	$this->input->post("uir_code_value"),
								"ticket"		=>	$ticket,
								"uir_class"		=>	$incident_name->main_name,
								"store_no"		=>	$store[count($store)-2],
								"store_name"	=>	$store[1],
								"uir_date"		=>	$this->input->post("date"),
								"uir_time"		=>	$this->input->post("time"),
								"mic"			=>	$mic[1],
								"sg"			=>	$sg[1],
								"summary"		=>	$this->input->post("summary"),
								"location"		=>	$this->input->post("location"),
								"initial_action"=>	$this->input->post("act"),
								"others"		=>	$data2
							);
						
						$message = generate_html_mail($email_data, TRUE);
						//add by jerico get recipient list
						$recipient_primary = get_recipient($this->input->post("sec_code_value"),1,$this->input->post("email"));
						//cc
						$store_name = utf8_decode($store[1]);//decode string for posible " ñ " string
						$recipient_secondary = get_recipient($this->input->post("sec_code_value"),2,0);
						$email_result = email($message, $recipient_primary, $recipient_secondary, $store_name."-".$incident_name->incident_name, TRUE);
						if($email_result) {
							$this->session->set_flashdata('successx', "UIR Successfully Saved! Email is sent!");
						} else {
							$this->session->set_flashdata('errorx', "Unable to send email!");
						}
					} else {
						$this->session->set_flashdata('errorx', "Unable to save details!");
						$this->uir_new_model->delete(array("uir_ticket_no"=>$ticket));
					}
				} else {
					$this->session->set_flashdata('errorx', "Unable to save to database!");
				}
			}
		}
		redirect(base_url().$this->module);
	}
	public function save_only()
	{
		if($_SERVER["REQUEST_METHOD"] === "GET") {
			$this->session->set_flashdata('errorx', "Invalid Request Method!");
			redirect(base_url().$this->module);
			exit();
		}
		//add validation here
		$this->load->library('form_validation');
		$this->form_validation->set_rules('uir_code_value', 'UIR Code', 'required');
		$this->form_validation->set_rules('incident', 'Incident', 'required');
		$this->form_validation->set_rules('date', 'Date of Discovery', 'required');
		$this->form_validation->set_rules('time', 'Time of Discovery', 'required');
		$this->form_validation->set_rules('store', 'Store', 'required');
		$this->form_validation->set_rules('rm_mic', 'RM/MIC', 'required');
		$this->form_validation->set_rules('sg', 'SG', 'required');
		$this->form_validation->set_rules('location', 'Incident Location', 'required');
		$this->form_validation->set_rules('act', 'Initial Action', 'required');
		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('errorx', validation_errors());
		} else {
		//end validation
			$store 	= explode("-", $this->input->post("store"));
			$pc 	= explode("#", $this->input->post("store"));
			$mic 	= explode("-", $this->input->post("rm_mic"));
			$sg 	= explode("-", $this->input->post("sg"));
			$uir  	= strtolower($this->input->post("uir_code_value"));
			$ticket = $this->generate_ticket_no();

			$this->load->model("uir_new_model");
			$this->load->model("uir_details_model");

			$data = array(
					"uir_ticket_no"		=>	$ticket,
					"incident_code"		=>	$this->input->post("incident"),
					"store_name_code"	=>	$store[0],
					"uir_date"			=>	$this->input->post("date"),
					"uir_time"			=>	$this->input->post("time"),
					"mic_code"			=>	$mic[0],
					"sg_code"			=>	$sg[0],
					"summary"			=>	$this->input->post("summary"),
					"location"			=>	$this->input->post("location"),
					"initial_action"	=>	$this->input->post("act"),
					"last_update"		=>	date("Y-m-d H:i:s"),
					"recipient"			=>	$this->input->post("email")
				);

			$ctr = 0;
			$data2 = array();
			foreach ($this->input->post() as $key => $value) {
				if(strpos($key, "label") !== FALSE) {
					$ctr++;
					$data2[] = array(
							"uir_ticket_no"		=>	$ticket,
							"label"				=>	$this->input->post("label_".$uir.$ctr),
							"value"				=>	$this->input->post("value_".$uir.$ctr)
						);
					//remove temporary the required field
					//$this->form_validation->set_rules("label_".$uir.$ctr, $this->input->post("label_".$uir.$ctr) . "Label", 'required');
					//$this->form_validation->set_rules("value_".$uir.$ctr, $this->input->post("label_".$uir.$ctr), 'required');
				}
			}
			if ($this->form_validation->run() === FALSE) {
				$this->session->set_flashdata('errorx', validation_errors());
			} else {
				$result = $this->uir_new_model->add($data);
				if($result === TRUE) {
					$result = $this->uir_details_model->add_batch($data2);
					if($result) {
						$this->load->model("incident_model");
						$incident_name = $this->incident_model->get_with_class(array("incident_code"=>$this->input->post("incident")))->result_object()[0];
						$email_data = array(
								"pc"			=>	$pc[1],
								"seccode"		=>	strtolower($this->input->post("sec_code_value")),
								"incident"		=>	$incident_name->incident_name,
								"uir_code"		=>	$this->input->post("uir_code_value"),
								"ticket"		=>	$ticket,
								"uir_class"		=>	$incident_name->main_name,
								"store_no"		=>	$store[count($store)-2],
								"store_name"	=>	$store[1],
								"uir_date"		=>	$this->input->post("date"),
								"uir_time"		=>	$this->input->post("time"),
								"mic"			=>	$mic[1],
								"sg"			=>	$sg[1],
								"summary"		=>	$this->input->post("summary"),
								"location"		=>	$this->input->post("location"),
								"initial_action"=>	$this->input->post("act"),
								"others"		=>	$data2
							);
					} else {
						$this->session->set_flashdata('errorx', "Unable to save details!");
						$this->uir_new_model->delete(array("uir_ticket_no"=>$ticket));
					}
				} else {
					$this->session->set_flashdata('errorx', "Unable to save to database!");
				}
			}
		}
		redirect(base_url().$this->module);
	}

	private function generate_ticket_no()
	{
		$this->load->model("uir_new_model");
		//$result = $this->uir_new_model->count_uir_today()->result_object()[0];
	//	$IdCount = $this->employee_model->num_rows();
		$result = $this->uir_new_model->num_rows();
		//$count = str_pad($result->COUNT+1, 6, "0", STR_PAD_LEFT);
		$count = str_pad($result+1, 6, "0", STR_PAD_LEFT);
		//return date("mdy")."-".$count;
		return $count;//change by jerico
	}


	public function edit($id)
	{
		$this->load->model("vtype_model");
		$data["uir_class"] = $this->vtype_model->get(array("deleted"=>"False"))->result_object();
		$this->admin_template_UIR("index", $data, "script","UPDATE");

	}
	

	public function view($ticket)
	{
		echo json_encode($this->{$this->model}->get_view(array("A.uir_ticket_no"=>$ticket))->result_object());
	}
	public function save_update($id)
	{
		if($_SERVER["REQUEST_METHOD"] === "GET") {
			$this->session->set_flashdata('errorx', "Invalid Request Method!");
			redirect(base_url().$this->module);
			exit();
		}
		//add validation here
		$this->load->library('form_validation');
		$this->form_validation->set_rules('uir_code_value', 'UIR Code', 'required');
		$this->form_validation->set_rules('incident', 'Incident', 'required');
		$this->form_validation->set_rules('date', 'Date of Discovery', 'required');
		$this->form_validation->set_rules('time', 'Time of Discovery', 'required');
		$this->form_validation->set_rules('store', 'Store', 'required');
		$this->form_validation->set_rules('rm_mic', 'RM/MIC', 'required');
		$this->form_validation->set_rules('sg', 'SG', 'required');
		$this->form_validation->set_rules('location', 'Incident Location', 'required');
		$this->form_validation->set_rules('act', 'Initial Action', 'required');
		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('errorx', validation_errors());
		} else {
		//end validation
			$store 	= explode("-", $this->input->post("store"));
			$pc 	= explode("#", $this->input->post("store"));
			$mic 	= explode("-", $this->input->post("rm_mic"));
			$sg 	= explode("-", $this->input->post("sg"));
			$uir  	= strtolower($this->input->post("uir_code_value"));
			$ticket = $id;

			$this->load->model("uir_new_model");
			$this->load->model("uir_details_model");

			$data = array(
					"uir_ticket_no"		=>	$id,
					"incident_code"		=>	$this->input->post("incident"),
					"store_name_code"	=>	$store[0],
					"uir_date"			=>	$this->input->post("date"),
					"uir_time"			=>	$this->input->post("time"),
					"mic_code"			=>	$mic[0],
					"sg_code"			=>	$sg[0],
					"summary"			=>	$this->input->post("summary"),
					"location"			=>	$this->input->post("location"),
					"initial_action"	=>	$this->input->post("act"),
					"last_update"		=>	date("Y-m-d H:i:s"),
					"recipient"			=>	$this->input->post("email")
				);

			$ctr = 0;
			$data2 = array();
			foreach ($this->input->post() as $key => $value) {
				if(strpos($key, "label") !== FALSE) {
					$ctr++;
					$data2[] = array(
							"uir_ticket_no"		=>	$id,
							"label"				=>	$this->input->post("label_".$uir.$ctr),
							"value"				=>	$this->input->post("value_".$uir.$ctr)
						);
					//remove temporary the required field
					//$this->form_validation->set_rules("label_".$uir.$ctr, $this->input->post("label_".$uir.$ctr) . "Label", 'required');
					//$this->form_validation->set_rules("value_".$uir.$ctr, $this->input->post("label_".$uir.$ctr), 'required');
				}
			}
			if ($this->form_validation->run() === FALSE) {
				$this->session->set_flashdata('errorx', validation_errors());
			} else {
				//edit uir here
				$result = $this->uir_new_model->edit(array("uir_ticket_no"=>$ticket),$data);
				if($result === TRUE) {
					//delete batch befere enter new
					$result = $this->uir_details_model->delete(array("uir_ticket_no"=>$ticket));
					$result = $this->uir_details_model->add_batch($data2);
					if($result) {
						$this->load->model("incident_model");
						$incident_name = $this->incident_model->get_with_class(array("incident_code"=>$this->input->post("incident")))->result_object()[0];
						
					} else {
						$this->session->set_flashdata('errorx', "Unable to save details!");
						$this->uir_new_model->delete(array("uir_ticket_no"=>$id));
					}
				} else {
					$this->session->set_flashdata('errorx', "Unable to save to database!");
				}
			}
		}
		redirect(base_url()."email");
	}
	
}


/* End of file home.php */
/* Location: ./application/controllers/home.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recipient_settings extends MY_Controller {

	public $model = "recipient_model";
	public $module = "recipient_settings";
	public $method = "search";
	public $limit = 10;

public function __construct()
	{
		parent::__construct();
		$this->title  = "RECIPIENT";
		$this->active = "recipient";
		if(! Sess::check_session()) {
			redirect(base_url()."login");
		}
	}
	public function index(){
	$this->load->model("recipient_model");
		$data["recipient_settings_class"] = $this->recipient_model->all()->result_object();
		$this->admin_template("index", $data, "script");

	}
	public function save(){
		if($_SERVER["REQUEST_METHOD"] === "GET") {
			$this->session->set_flashdata('errorx', "Invalid Request Method!");
			redirect(base_url().$this->module);
			exit();
		}
		$this->load->library('form_validation');
		$this->form_validation->set_rules('value_secode', 'Security Code', 'required');
		$this->form_validation->set_rules('value_type', 'Type', 'required');
		$this->form_validation->set_rules('emailadd', 'Email Add', 'required');
				if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('errorx', validation_errors());
		} else {
			$email_list = explode(",", $this->input->post("emailadd"));
			$color = $this->input->post("value_secode");
			$type = $this->input->post("value_type");
			$this->load->model("recipient_model");
			for ($i=0; $i <= count($email_list) - 1; $i++) { 
			//echo ;
			//echo "</br>";
				//insert to db email address
				$data = array(
					'color' => $color,
					'email' => $email_list[$i],
					'status' => 0,
					'type' => $type
					);
				$this->recipient_model->add($data);
			 } ;
		}
	redirect(base_url().$this->module);
	}
	public function get_email_list($sortby, $orderby, $filter){
		if ($sortby === "4"){$sortby = "color,type";}elseif ($sortby === "0") {$sortby = "id";}
		if ($orderby === "0"){$orderby = "DESC";}
		if ($filter === "0"){
			$filter = "";
		}else{
				$filter = explode("-", $filter);
				$filter = "AND ".$filter[0]." LIKE '%".$filter[1]."%'";
			}
		$this->load->model("recipient_model");
		$data = $this->recipient_model->get("status <> 1 ".$filter." order by ".$sortby." ".$orderby."")->result_object();
		echo json_encode($data);	
	
	}
	public function delete($id){
		$this->load->model("recipient_model");
		$data = array(
				'status' => '1'
			);
		$result = $this->recipient_model->edit("id = '$id'",$data);
		echo json_encode($result);
	}

	// public function get_details($search)
	// {
	// 	if($search === "MT") {
	// 		$details = $this->load->view($this->module."/managers_theft", '', true);
	// 	} else {
	// 		$details = "";
	// 	}
		
	// 	echo json_encode(array("data"=>$details));
	// }

	


}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
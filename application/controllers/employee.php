<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee extends MY_Controller {

	public $model = "employee_model";
	public $module = "employee";
	public $method = "item";
	public $limit = 10;

public function __construct()
	{
		parent::__construct();
		$this->title  = "EMPLOYEE";
		$this->active = "maintenance";
		if(! Sess::check_session()) {
			redirect(base_url()."login");
		}
		//$this->load->model($this->model);
	}
	public function index(){
		$this->load->model("employee_model");
		$view["data"] = $this->employee_model->get("deleted like 'False'LIMIT 10")->result_object();

		$this->admin_template("index", $view, "script");
		//$this->admin_template("index", $data, "script");
	}
	public function sort(){
		if($_SERVER["REQUEST_METHOD"] === "GET") {
			$this->session->set_flashdata('errorx', "Invalid Request Method!");
			redirect(base_url().$this->module);
			exit();
		}
		//add validation here
		$this->load->library('form_validation');
		$this->form_validation->set_rules('value_stype', 'Employee Type', 'required');
		$this->form_validation->set_rules('value_searchby', 'Serch By', 'required');
		$this->form_validation->set_rules('value_kword', 'Keywords', 'required');
		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('errorx', validation_errors());
			redirect(base_url().$this->module);
		}else
		{
			$keyword = $this->input->post("value_kword");
		//$view["data"] = $this->employee_model->get("deleted like 'False'LIMIT 10")->result_object();
		
		switch ($this->input->post("value_searchby")) {
			case '0'://name
					switch ($this->input->post("value_stype")) {
						case '0'://crew
						$select = "employee.emp_code,employee.emp_name,store.store_name,employee.emp_fname,employee.emp_sname,employee.emp_initial";
						$where = "emp_name like '%".$keyword."%' AND employee.deleted like 'False'";
						$join = array("store","store.store_code = employee.store_code");//set join
						$this->load->model("employee_model");
						$view["data"] = $this->employee_model->get_join($where,$join,$select)->result_object();
						$view["fields"] = array("emp_code","emp_name","store_name","emp_fname","emp_initial","emp_sname"); 
						break;
						case '1'://mic
						$select = "mic.mic_code,mic.mic_name,store.store_name,mic.mic_fname,mic.mic_sname,mic.mic_initial";
						$where = "mic_name like '%".$keyword."%' AND mic.deleted like 'False'";
						$join = array("store","store.store_code = mic.store_name");//set join
						$this->load->model("mic_model");
						$view["data"] = $this->mic_model->get_join($where,$join,$select)->result_object();
						$view["fields"] = array("mic_code","mic_name","store_name","mic_fname","mic_initial","mic_sname"); 
						break;
						case '2'://sg
						$select = "sg.sg_code,sg.sg_name,store.store_name,sg.sg_fname,sg.sg_sname,sg.sg_initial";
						$where = "sg_name like '%".$keyword."%' AND sg.deleted like 'False'";
						$join = array("store","store.store_code = sg.store_code");//set join
						$this->load->model("sg_model");
						$view["data"] = $this->sg_model->get_join($where,$join,$select)->result_object();
						$view["fields"] = array("sg_code","sg_name","store_name","sg_fname","sg_initial","sg_sname"); 
						break;
					}
				$view["type"] = $this->input->post("value_stype");
				$view["search"] = "Result by searching Name keyword like '" . $keyword . "'";
				$this->admin_template("index", $view, "script");
				break;
				case '1'://store
				switch ($this->input->post("value_stype")) 
				{
						case '0'://crew
						$select = "employee.emp_code,employee.emp_name,store.store_name,employee.emp_fname,employee.emp_sname,employee.emp_initial";
						$where = "store_name like '%".$keyword."%'";
						$join = array("employee","employee.store_code = store.store_code AND employee.deleted like 'False'");//set join
						$this->load->model("store_model");
						$view["data"] = $this->store_model->get_join($where,$join,$select)->result_object();
						$view["fields"] = array("emp_code","emp_name","store_name","emp_fname","emp_initial","emp_sname"); 
						break;
						case '1'://mic
						$select = "mic.mic_code,mic.mic_name,store.store_name,mic.mic_fname,mic.mic_sname,mic.mic_initial";
						$where = "store.store_name like '%".$keyword."%'";
						$join = array("mic","mic.store_name = store.store_code AND mic.deleted like 'False'");//set join
						$this->load->model("store_model");
						$view["data"] = $this->store_model->get_join($where,$join,$select)->result_object();
						$view["fields"] = array("mic_code","mic_name","store_name","mic_fname","mic_initial","mic_sname"); 
						break;
						case '2'://sg
						$select = "sg.sg_code,sg.sg_name,store.store_name,sg.sg_fname,sg.sg_sname,sg.sg_initial";
						$where = "store_name like '%".$keyword."%'";
						$join = array("sg","sg.store_code = store.store_code AND sg.deleted like 'False'");//set join
						$this->load->model("store_model");
						$view["data"] = $this->store_model->get_join($where,$join,$select)->result_object();
						$view["fields"] = array("sg_code","sg_name","store_name","sg_fname","sg_initial","sg_sname"); 
						break;
				}
				$view["type"] = $this->input->post("value_stype");
				$view["search"] = "Result by searching Store keyword like '" . $keyword . "'";
				$this->admin_template("index", $view, "script");
				break;
		//$this->load->model("employee_model");
		
		}//switch

	}//end if
		
}//function
	public function get_employee($search)
	{
		
		$this->load->model("employee_model");
		$data = $this->employee_model->get("deleted = 'FALSE' AND (emp_code LIKE '%{$search}%' OR emp_fname LIKE '%{$search}%' OR emp_sname LIKE '%{$search}%') LIMIT 10")->result_object();
		echo json_encode($data);
	}
	public function get_mic($search)
	{
		$this->load->model("mic_model");
		$data = $this->mic_model->get("deleted = 'FALSE' AND (mic_name LIKE '%{$search}%' OR mic_code LIKE '%{$search}%') LIMIT 10")->result_object();
		echo json_encode($data);
	}
	public function get_sg($search)
	{
		$this->load->model("sg_model");
		$data = $this->sg_model->get("deleted = 'FALSE' AND (sg_name LIKE '%{$search}%' OR sg_code LIKE '%{$search}%') LIMIT 10")->result_object();
		echo json_encode($data);
	}
	public function get_store($search)
	{
		$this->load->model("store_model");
		$data = $this->store_model->get("deleted = 'FALSE' AND (store_name LIKE '%{$search}%' OR store_code LIKE '%{$search}%') LIMIT 10")->result_object();
		echo json_encode($data);
	}
	public function transfer_emp()
	{
		if($_SERVER["REQUEST_METHOD"] === "GET") {
			$this->session->set_flashdata('errorx', "Invalid Request Method!");
			redirect(base_url().$this->module);
			exit();
		}
		$storeno = explode("-", $this->input->post("value_store"));
		$emp_code = explode("-", $this->input->post("value_name"));
		switch ($this->input->post("value_type"))
		{
			case 0:
			$this->load->model("employee_model");
			$data = array(
				"store_code" => $storeno[0]
				);
			$result = $this->employee_model->edit("emp_code = '$emp_code[0]'",$data);
			break;
			case 1:
			$this->load->model("mic_model");
			$data = array(
				"store_name" => $storeno[0]
				);
			$result = $this->mic_model->edit("mic_code = '$emp_code[0]'",$data);
			break;
			case 2:
			$this->load->model("sg_model");
			$data = array(
				"store_code" => $storeno[0]
				);
			$result = $this->sg_model->edit("sg_code = '$emp_code[0]'",$data);
			break;
		}
		redirect(base_url().$this->module);
	}
	public function get_details($search)
	{
		if($search === "0") {
			$details = $this->load->view($this->module."/new_emp", '', true);
		
		}
		elseif($search === "1"){
			$details = $this->load->view($this->module."/transfer", '', true);
		}
		echo json_encode(array("data"=>$details));
	}
	public function save()
	{
		if($_SERVER["REQUEST_METHOD"] === "GET") {
			$this->session->set_flashdata('errorx', "Invalid Request Method!");
			redirect(base_url().$this->module);
			exit();
		}
		$storeno = explode("-", $this->input->post("value_store"));
		switch ($this->input->post("value_type"))
		{
			case 0:
			$this->load->model("employee_model");
			
			$IdCount = $this->employee_model->num_rows();
		    $my_id = str_pad($IdCount+1, 7, "0", STR_PAD_LEFT);//generate format ID
		    $emp_name = $this->input->post("value_fname")." ".$this->input->post("value_lname");
			$data = array(
				"emp_code" 	 	=> "E".$my_id,
				"emp_name"	 	=> $emp_name,
				"store_code" 	=> $storeno[0],
				"emp_sname"  	=> $this->input->post("value_lname"),
				"emp_fname"   	=> $this->input->post("value_fname"),
				"emp_initial"   => $this->input->post("value_mi"),
				"emp_shortname" => $this->input->post("value_sname")
				);
			$result = $this->employee_model->add($data);
			break;
			case 1:
			$this->load->model("mic_model");
			$IdCount = $this->mic_model->num_rows();
		    $my_id = str_pad($IdCount+1, 6, "0", STR_PAD_LEFT);//generate format ID
		    $emp_name = $this->input->post("value_fname")." ".$this->input->post("value_lname");
			$data = array(
				"mic_code"		=>"M".$my_id,
				"mic_name"		=>$emp_name,
				"store_name" 	=>$storeno[0],
				"mic_sname"		=>$this->input->post("value_lname"),
				"mic_fname"		=>$this->input->post("value_fname"),
				"mic_initial"	=>$this->input->post("value_mi"),
				"mic_shortname"	=>$this->input->post("value_sname")
				);
			$result = $this->mic_model->add($data);
			break;
			case 2:
			$this->load->model("sg_model");
			$IdCount = $this->sg_model->num_rows();
		    $my_id = str_pad($IdCount+1, 6, "0", STR_PAD_LEFT);//generate format ID
		    $emp_name = $this->input->post("value_fname")." ".$this->input->post("value_lname");
			$data = array(
				"sg_code"	  => "SG".$my_id,
				"sg_name"	  =>$emp_name,
				"store_code"  =>$storeno[0],
				"sg_sname"	  =>$this->input->post("value_lname"),
				"sg_fname"	  =>$this->input->post("value_fname"),
				"sg_initial"  =>$this->input->post("value_mi"),
				"sg_shortname"=>$this->input->post("value_sname")
				);
			$result = $this->sg_model->add($data);
			break;
		}
		if($result){
			$this->session->set_flashdata('successx', "New Employee Successfully Saved!");
		}else{
			$this->session->set_flashdata('errorx', "Unable to save new employee record!");
		}
		redirect(base_url().$this->module);
	}

	public function delete($type, $empcode){
	switch ($type) {
		case 0:
			$data = array(
				"deleted" => "True"
				);
			$this->load->model("employee_model");
			$result = $this->employee_model->edit("emp_code = '$empcode'",$data);
			break;
		case 1:
			$data = array(
				"deleted" => "True"
				);
			$this->load->model("mic_model");
			$result = $this->mic_model->edit("mic_code = '$empcode'",$data);
			break;
		case 2:
			$data = array(
				"deleted" => "True"
				);
			$this->load->model("sg_model");
			$result = $this->sg_model->edit("sg_code = '$empcode'",$data);
			break;

				}
    echo json_encode($result);
	}

	public function update()
	{
		if($_SERVER["REQUEST_METHOD"] === "GET") {
			$this->session->set_flashdata('errorx', "Invalid Request Method!");
			redirect(base_url().$this->module);
			exit();
		}
		$this->load->library('form_validation');
		$this->form_validation->set_rules('update_fname', 'First Name', 'required');
	
		
		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('errorx', validation_errors());
			redirect(base_url().$this->module);
		}else{
		$type = $this->input->post("emp_type");
				$empcode = $this->input->post("emp_code");
				$employee_name = $this->input->post("update_fname")." ".$this->input->post("update_lname");
				switch ($type) {
					case 0:
						$this->load->model("employee_model");
						$data = array(
							'emp_fname' 	=> $this->input->post("update_fname"),
							'emp_sname' 	=> $this->input->post("update_lname"),
							'emp_initial' 	=> $this->input->post("update_mi"),
							'emp_name'		=> $employee_name,
							'emp_shortname' => $this->input->post("update_sname")
							);
						$return = $this->employee_model->edit(array("emp_code" => $empcode),$data);
						break;
					case 1:
					$this->load->model("mic_model");
					$data = array(
							'mic_fname' 	=> $this->input->post("update_fname"),
							'mic_sname' 	=> $this->input->post("update_lname"),
							'mic_initial' 	=> $this->input->post("update_mi"),
							'mic_name'		=> $employee_name,
							'mic_shortname' => $this->input->post("update_sname")
							);
						$return = $this->mic_model->edit(array("mic_code" => $empcode),$data);
						break;
					case 2:
						$this->load->model("sg_model");
							$data = array(
							'sg_fname' 	=> $this->input->post("update_fname"),
							'sg_sname' 	=> $this->input->post("update_lname"),
							'sg_initial' 	=> $this->input->post("update_mi"),
							'sg_name'		=> $employee_name,
							'sg_shortname' => $this->input->post("update_sname")
							);
						$return = $this->sg_model->edit(array("sg_code" => $empcode),$data);
						break;	
				}
				if($return){
			$this->session->set_flashdata('successx', "Successfully Update!");
			redirect(base_url().$this->module);
				//redirect(base_url().$this->module);
				}else
				{
			$this->session->set_flashdata('errorx', "Error in updating record!");
			redirect(base_url().$this->module);
			exit();
				}
		}
		
	}
	public function get_emp_info($type, $code)
	{
		switch ($type) {
			case 0:
				$this->load->model("employee_model");
				$result = $this->employee_model->get("emp_code = '$code'")->result_object();
				break;
			case 1:
				$this->load->model("mic_model");
				$result = $this->mic_model->get("mic_code = '$code'")->result_object();
				break;
			case 2:
				$this->load->model("sg_model");
				$result = $this->sg_model->get("sg_code = '$code'")->result_object();
				break;
		}
		echo json_encode($result);
	}


}


/* End of file home.php */
/* Location: ./application/controllers/home.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->title  = "HOME";
		$this->active = "dashboard";
		$this->module = "home";
		if(! Sess::check_session()) {
			redirect(base_url()."login");
		}
	}

	public function index()
	{
		$this->load->model("vtype_model");
		$this->load->model("uir_new_model");
		$data["vtype"] = $this->vtype_model->get_stats()->result_object();
		$data["overall_count"] = $this->uir_new_model->count();
		$data["month"] = $this->vtype_model->get_stats("MONTHLY")->result_object();
		$data["month_count"] = $this->uir_new_model->count("MONTHLY");
		$data["daily"] = $this->vtype_model->get_stats("DAILY")->result_object();
		$data["daily_count"] = $this->uir_new_model->count("DAILY");
		$data["uir"] = $this->uir_new_model->get_uir(FALSE, TRUE)->result_object();
		$this->admin_template("index", $data, "script");
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
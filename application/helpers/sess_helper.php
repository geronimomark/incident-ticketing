<?php

class Sess {

	Static function check_session()
	{
		$CI =& get_instance();
		if(! $CI->session->userdata("name")) {
			$CI->session->set_flashdata("error", "Please login to continue...");
			return FALSE;
		}
		return TRUE;
	}

}
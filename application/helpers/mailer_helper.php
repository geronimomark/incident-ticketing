<?php
	function get_recipient($uir_color,$type,$other_recipient)
	{
		$CI =& get_instance();
		$recipient = "";
		$CI->load->model("recipient_model");
		$data = $CI->recipient_model->get("color like '$uir_color' AND status <> 1 AND type = $type");
		if ($data->num_rows() > 0){
			$i=0;
			foreach ($data->result_array() as $row)
			{
				$recipient = $recipient."".$row['email'];
				$i++;
				if ($i != $data->num_rows())
				{
					$recipient = $recipient.", ";
				}
			}
			
		}
		if ($other_recipient != "0" AND $type === 1)
		{
			if ($recipient != "")
			{
				$recipient = $recipient.", ".$other_recipient;
			}else
			{
				$recipient = $other_recipient;
			}
			
		}
		return $recipient;
	}
	function email($message, $primary, $secondary, $subject, $resend = FALSE)
	{
		$config = Array(
			'protocol' 	=> 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => "465",
			'smtp_user' => 'secgrandeur@gmail.com', // change it to yours
			'smtp_pass' => 'grandone1', // change it to yours
			'mailtype' 	=> 'html',
			'charset' 	=> 'iso-8859-1',
			'wordwrap' 	=> TRUE
		);

		$CI =& get_instance();

		$CI->load->library('email', $config);
		$CI->email->set_newline("\r\n");
		$CI->email->from('secgrandeur@gmail.com'); // change it to yours
		$CI->email->to($primary);// change it to yours
		$CI->email->cc($secondary);
		if($resend)
			$CI->email->subject($subject);
		else
			$CI->email->subject($subject);
		$CI->email->message($message);
		if($CI->email->send()) {
			// echo 'Email sent.';
			return TRUE;
		} else {
			// show_error($this->email->print_debugger());
			return FALSE;
		}
	}
	function get_list_item($data)
	{	
		$items = "";
		$data = explode(",", $data);
		foreach ($data as $key => $value) 
		{
		 	$items = $items."<i>".$value."</i><br />";		 
		 } 
	return $items;
	}

	function generate_html_mail($data, $resend = FALSE)
	{
		$label1 = isset($data["others"][0]["label"]) ? strtoupper($data["others"][0]["label"]).":" : '';
		$label2 = isset($data["others"][1]["label"]) ? strtoupper($data["others"][1]["label"]).":" : '';
		$label3 = isset($data["others"][2]["label"]) ? strtoupper($data["others"][2]["label"]).":" : '';
		$label4 = isset($data["others"][3]["label"]) ? strtoupper($data["others"][3]["label"]).":" : '';
		$label5 = isset($data["others"][4]["label"]) ? strtoupper($data["others"][4]["label"]).":" : '';
		$label6 = isset($data["others"][5]["label"]) ? strtoupper($data["others"][5]["label"]).":" : '';
		$value1 = isset($data["others"][0]["value"]) ? $data["others"][0]["value"] : '';
		$value2 = isset($data["others"][1]["value"]) ? $data["others"][1]["value"] : '';
		$value3 = isset($data["others"][2]["value"]) ? $data["others"][2]["value"] : '';
		$value4 = isset($data["others"][3]["value"]) ? $data["others"][3]["value"] : '';
		$value5 = isset($data["others"][4]["value"]) ? $data["others"][4]["value"] : '';
		$value6 = isset($data["others"][5]["value"]) ? $data["others"][5]["value"] : '';
		if ($data["uir_code"] === "USA" OR $data["uir_code"] === "CFV" OR $data["uir_code"] === "BPV" OR $data["uir_code"] === "VKY-VKEYS" OR $data["uir_code"] === "PIL" OR $data["uir_code"] === "TSC" OR $data["uir_code"] === "UW" OR $data["uir_code"] === "W" OR $data["uir_code"] === "BPI" OR $data["uir_code"] === "NIT-NITT" OR $data["uir_code"] === "BPV" OR $data["uir_code"] === "ET" OR $data["uir_code"] === "CTF" OR $data["uir_code"] === "EO" OR $data["uir_code"] === "OI" OR $data["uir_code"] === "PI" OR $data["uir_code"] === "MSC" OR $data["uir_code"] === "EIP" OR $data["uir_code"] === "PI" OR $data["uir_code"] === "D8C" OR $data["uir_code"] === "DTP" OR $data["uir_code"] === "GDC" OR $data["uir_code"] === "MT" OR $data["uir_code"] === "L-O" OR $data["uir_code"] === "UV" OR $data["uir_code"] === "BIV" OR $data["uir_code"] === "PUTD-UTD" OR $data["uir_code"] === "8MW" OR $data["uir_code"] === "USA" OR $data["uir_code"] === "FSC-FSCC" OR $data["uir_code"] === "HD" OR $data["uir_code"] === "UCO" OR $data["uir_code"] === "HD" OR $data["uir_code"] === "EMV" OR $data["uir_code"] === "L&K-LAK" OR $data["uir_code"] === "SV" OR $data["uir_code"] === "F" OR $data["uir_code"] === "TRT" OR $data["uir_code"] === "DAI-D-AI" OR $data["uir_code"] === "H" OR $data["uir_code"] === "BT" OR $data["uir_code"] === "BK" OR $data["uir_code"] === "SC" OR $data["uir_code"] === "BB" OR $data["uir_code"] === "C" OR $data["uir_code"] === "PDW-IPDW")
		{
			$layout = 1;
		}elseif ($data["uir_code"] === "FBD-FBV" OR $data["uir_code"] === "FBV" OR $data["uir_code"] === "EOD" OR $data["uir_code"] === "NIR" OR $data["uir_code"] === "VMS" OR $data["uir_code"] === "PT" OR $data["uir_code"] === "CS" OR $data["uir_code"] === "CO" OR $data["uir_code"] === "CCV" OR $data["uir_code"] === "LAF" OR $data["uir_code"] === "R") {
			$layout = 2;
		}elseif ($data["uir_code"] === "OA" OR $data["uir_code"] === "RA" OR $data["uir_code"] === "VA" OR $data["uir_code"] === "SAF" OR $data["uir_code"] === "B") {
			$layout = 3;
		}elseif ($data["uir_code"] === "TDP" OR $data["uir_code"] === "T"){
			$layout = 4;
		}elseif ($data["uir_code"] === "CCTV-CTV" OR $data["uir_code"] === "DE-DEE" OR $data["uir_code"] === "FE" OR $data["uir_code"] === "FBD" OR $data["uir_code"] === "L" OR $data["uir_code"] === "ALM-EAS" ){
			$layout = 5;
		}
		switch($layout)
		{
			case 1:
			$layout = '<table width="641" border="1" cellspacing="0" style="font-size:12px;font-family:Arial, Helvetica, sans-serif">
			<tr>
			<td colspan="7" bgcolor="#999999"><h2>UIR TICKET</h2></td>
			</tr>
			<tr>
			<td width="101"><strong>SECURITY CODE:</strong></td>
			<td colspan="2" bgcolor="'.$data["seccode"].'"><div style="color:#FFF" align="center">'.strtoupper($data["seccode"]).'</div></td>
			<td width="98"><strong>INCIDENT:</strong></td>
			<td width="120">'.$data["incident"].'</td>
			<td width="61"><strong>UIR CODE</strong></td>
			<td width="119"><div align="center">'.$data["uir_code"].'</div></td>
			</tr>
			<tr>
			<td><strong>UIR TICKET #:</strong></td>
			<td colspan="2">'.$data["ticket"].'</td>
			<td><strong>UIR CLASS:</strong></td>
			<td>'.$data["uir_class"].'</td>
			<td rowspan="6"><div align="center"><strong>'.$label3.'</strong></div></td>
			<td rowspan="6" style="vertical-align:text-top">'.get_list_item($value3).'</td>
			</tr>
			<tr>
			<td><strong>STORE #:</strong></td>
			<td width="56">'.$data["store_no"].'</td>
			<td width="56"><strong>PC#'.$data["pc"].'</strong></td>
			<td><strong>STORE NAME:</strong></td>
			<td>'.$data["store_name"].'</td>
			</tr>
			<tr>
			<td><strong>DATE OF INCIDENT:</strong></td>
			<td colspan="2">'.$data["uir_date"].'</td>
			<td><strong>TIME OF INCIDENT:</strong></td>
			<td>'.$data["uir_time"].'</td>
			</tr>
			<tr>
			<td><strong>RM/MIC ON DUTY:</strong></td>
			<td colspan="2">'.$data["mic"].'</td>
			<td><strong>NAME OF GUARDS:</strong></td>
			<td>'.$data["sg"].'</td>
			</tr>
			<tr>
			<td><strong>'.$label1.'</strong></td>
			<td colspan="2">'.$value1.'</td>
			<td><strong>'.$label2.'</strong></td>
			<td>'.$value2.'</td>
			</tr>
			<tr>
			<td colspan="3"><strong>BRIEF SUMMARY :</strong></td>
			<td><strong>LOCATION:</strong></td>
			<td>'.$data["location"].'</td>
			</tr>
			<tr>
			<td colspan="7">'.$data["summary"].'</td>
			</tr>
			<tr>
			<td colspan="7"><strong>INITIAL ACTION TAKEN/DONE:</strong></td>
			</tr>
			<tr>
			<td colspan="7">'.$data["initial_action"].'</td>
			</tr>
			</table>';
			break;
			case 2:
			$layout = '<table width="641" border="1" cellspacing="0" style="font-size:12px;font-family:Arial, Helvetica, sans-serif">
			<tr>
			<td colspan="7" bgcolor="#999999"><h2>UIR TICKET</h2></td>
			</tr>
			<tr>
			<td width="101"><strong>SECURITY CODE:</strong></td>
			<td colspan="2" bgcolor="'.$data["seccode"].'"><div style="color:#FFF" align="center">'.strtoupper($data["seccode"]).'</div></td>
			<td width="98"><strong>INCIDENT:</strong></td>
			<td width="120">'.$data["incident"].'</td>
			<td width="61"><strong>UIR CODE</strong></td>
			<td width="119"><div align="center">'.$data["uir_code"].'</div></td>
			</tr>
			<tr>
			<td><strong>UIR TICKET #:</strong></td>
			<td colspan="2">'.$data["ticket"].'</td>
			<td><strong>UIR CLASS:</strong></td>
			<td>'.$data["uir_class"].'</td>
			<td rowspan="3"><div align="center"><strong>'.$label3.'</strong></div></td>
			<td rowspan="3">'.$value3.'</td>
			</tr>
			<tr>
			<td><strong>STORE #:</strong></td>
			<td width="56">'.$data["store_no"].'</td>
			<td width="56"><strong>'.$data["pc"].'</strong></td>
			<td><strong>STORE NAME:</strong></td>
			<td>'.$data["store_name"].'</td>
			</tr>
			<tr>
			<td><strong>DATE OF INCIDENT:</strong></td>
			<td colspan="2">'.$data["uir_date"].'</td>
			<td><strong>TIME OF INCIDENT:</strong></td>
			<td>'.$data["uir_time"].'</td>
			</tr>
			<tr>
			<td><strong>RM/MIC ON DUTY:</strong></td>
			<td colspan="2">'.$data["mic"].'</td>
			<td><strong>NAME OF GUARDS:</strong></td>
			<td>'.$data["sg"].'</td>
			<td rowspan="3"><div align="center"><strong>'.$label4.'</strong></div></td>
			<td rowspan="3">'.get_list_item($value4).'</td>
			</tr>
			<tr>
			<td><strong>'.$label1.'</strong></td>
			<td colspan="2">'.$value1.'</td>
			<td><strong>'.$label2.'</strong></td>
			<td>'.$value2.'</td>
			</tr>
			<tr>
			<td colspan="3"><strong>BRIEF SUMMARY :</strong></td>
			<td><strong>LOCATION:</strong></td>
			<td>'.$data["location"].'</td>
			</tr>
			<tr>
			<td colspan="7">'.$data["summary"].'</td>
			</tr>
			<tr>
			<td colspan="7"><strong>INITIAL ACTION TAKEN/DONE:</strong></td>
			</tr>
			<tr>
			<td colspan="7">'.$data["initial_action"].'</td>
			</tr>
			</table>'; 
			break;
			case 3:
			$layout = '<table width="641" border="1" cellspacing="0" style="font-size:12px;font-family:Arial, Helvetica, sans-serif">
			  <tr>
			    <td colspan="7" bgcolor="#999999"><h2>UIR TICKET</h2></td>
			  </tr>
			  <tr>
			    <td width="101"><strong>SECURITY CODE:</strong></td>
			    <td colspan="2" bgcolor="'.$data["seccode"].'"><div style="color:#FFF" align="center">'.strtoupper($data["seccode"]).'</div></td>
			    <td width="110"><strong>INCIDENT:</strong></td>
			    <td width="150">'.$data["incident"].'</td>
			    <td width="66"><strong>UIR CODE</strong></td>
			    <td width="72"><div align="center">'.$data["uir_code"].'</div></td>
			  </tr>
			  <tr>
			    <td><strong>UIR TICKET #:</strong></td>
			    <td colspan="2">'.$data["ticket"].'</td>
			    <td><strong>UIR CLASS:</strong></td>
			    <td>'.$data["uir_class"].'</td>
			    <td>'.$label2.'</td>
			    <td>'.$value2.'</td>

			  </tr>
			  <tr>
			    <td><strong>STORE #:</strong></td>
			    <td width="56">'.$data["store_no"].'</td>
			    <td width="56">PC'.$data["pc"].'</td>
			    <td><strong>STORE NAME:</strong></td>
			   	<td>'.$data["store_name"].'</td>
			    <td colspan="2" rowspan="2"><div align="center">Injured part</div><div align="center"></div></td>
			  </tr>
			  <tr>
			    <td><strong>DATE OF INCIDENT:</strong></td>
			    <td colspan="2">'.$data["uir_date"].'</td>
			    <td><strong>TIME OF INCIDENT:</strong></td>
			    <td>'.$data["uir_time"].'</td>
			  </tr>
			  <tr>
			    <td><strong>RM/MIC ON DUTY:</strong></td>
			    <td colspan="2">'.$data["mic"].'</td>
			    <td><strong>NAME OF GUARDS:</strong></td>
			    <td>'.$data["sg"].'</td>
			    <td><div align="center">'.$label3.'</div></td>
			    <td>'.$value3.'</td>			   
			  </tr>
			  <tr>
			    <td><strong>NAME OF VICTIM/ PATIENT:</strong></td>
			    <td colspan="2">'.$value1.'</td>
			    <td><strong>'.$label6.'</strong></td>
			    <td>'.$value6.'</td>
			    <td><div align="center">'.$label4.'</div></td>
			    <td >'.$value4.'</td>
			  </tr>
			  <tr>
			    <td colspan="3"><strong>BRIEF SUMMARY :</strong></td>
			    <td><strong>LOCATION OF ACCIDENT:</strong></td>
			    <td>'.$data["location"].'</td>
			    <td><div align="center">'.$label5.'</div></td>
			    <td>'.$value5.'</td>
			  </tr>
			  <tr>
			    <td colspan="7">'.$data["summary"].'</td>
			  </tr>
			  <tr>
			    <td colspan="7"><strong>INITIAL ACTION TAKEN/DONE:</strong></td>
			  </tr>
			  <tr>
			    <td colspan="7">'.$data["initial_action"].'</td>
			  </tr>
			</table>';
			break;
			case 4:
			$layout = '<table width="641" border="1" cellspacing="0" style="font-size:12px;font-family:Arial, Helvetica, sans-serif">
			<tr>
			<td colspan="7" bgcolor="#999999"><h2>UIR TICKET</h2></td>
			</tr>
			<tr>
			<td width="101"><strong>SECURITY CODE:</strong></td>
			<td colspan="2" bgcolor="'.$data["seccode"].'"><div style="color:#FFF" align="center">'.strtoupper($data["seccode"]).'</div></td>
			<td width="98"><strong>INCIDENT:</strong></td>
			<td width="120">'.$data["incident"].'</td>
			<td width="61"><strong>UIR CODE</strong></td>
			<td width="119"><div align="center">'.$data["uir_code"].'</div></td>
			</tr>
			<tr>
			<td><strong>UIR TICKET #:</strong></td>
			<td colspan="2">'.$data["ticket"].'</td>
			<td><strong>UIR CLASS:</strong></td>
			<td>'.$data["uir_class"].'</td>
			<td rowspan="3"><div align="center"><strong>'.get_list_item($label3).'</strong></div></td>
			<td rowspan="3">'.get_list_item($value3).'</td>
			</tr>
			<tr>
			<td><strong>STORE #:</strong></td>
			<td width="56">'.$data["store_no"].'</td>
			<td width="56"><strong>PC#'.$data["pc"].'</strong></td>
			<td><strong>STORE NAME:</strong></td>
			<td>'.$data["store_name"].'</td>
			</tr>
			<tr>
			<td><strong>DATE OF INCIDENT:</strong></td>
			<td colspan="2">'.$data["uir_date"].'</td>
			<td><strong>TIME OF INCIDENT:</strong></td>
			<td>'.$data["uir_time"].'</td>
			</tr>
			<tr>
			<td><strong>RM/MIC ON DUTY:</strong></td>
			<td colspan="2">'.$data["mic"].'</td>
			<td><strong>NAME OF GUARDS:</strong></td>
			<td>'.$data["sg"].'</td>
			<td rowspan="3"><div align="center"><strong>'.$label4.'</strong></div></td>
			<td rowspan="3">'.get_list_item($value4).'</td>
			</tr>
			<tr>
			<td><strong>'.$label1.'</strong></td>
			<td colspan="2">'.$value1.'</td>
			<td><strong>'.$label2.'</strong></td>
			<td>'.$value2.'</td>
			</tr>
			<tr>
			<td colspan="3"><strong>BRIEF SUMMARY :</strong></td>
			<td><strong>LOCATION:</strong></td>
			<td>'.$data["location"].'</td>
			</tr>
			<tr>
			<td colspan="7">'.$data["summary"].'</td>
			</tr>
			<tr>
			<td colspan="7"><strong>INITIAL ACTION TAKEN/DONE:</strong></td>
			</tr>
			<tr>
			<td colspan="7">'.$data["initial_action"].'</td>
			</tr>
			</table>'; 
			break;
			case 5:
			$layout = '<table width="641" border="1" cellspacing="0" style="font-size:12px;font-family:Arial, Helvetica, sans-serif">
			  <tr>
			    <td colspan="7" bgcolor="#999999"><h2>UIR TICKET</h2></td>
			  </tr>
			  <tr>
			    <td width="111"><strong>SECURITY CODE:</strong></td>
			    <td colspan="2" bgcolor="'.$data["seccode"].'"><div style="color:#FFF" align="center">'.strtoupper($data["seccode"]).'</div></td>
			    <td width="106"><strong>INCIDENT:</strong></td>
			    <td width="133">'.$data["incident"].'</td>
			    <td width="101"><strong>UIR CODE</strong></td>
			    <td width="73"><div align="center">'.$data["uir_code"].'</div></td>
			  </tr>
			  <tr>
			    <td><strong>UIR TICKET #:</strong></td>
			    <td colspan="2">'.$data["ticket"].'</td>
			    <td><strong>UIR CLASS:</strong></td>
			     <td>'.$data["uir_class"].'</td>
			    <td rowspan="5">
			   	<div align="center"><strong>'.get_list_item($label3).'</strong></div>
			    </td>
			    <td rowspan="5">'.$value3.'</td>
			  </tr>
			  <tr>
			    <td><strong>STORE #:</strong></td>
			    <td width="42">'.$data["store_no"].'</td>
			    <td width="45"><strong>PC#'.$data["pc"].'</strong></td>
			    <td><strong>STORE NAME:</strong></td>
			    <td>'.$data["store_name"].'</td>
			  </tr>
			  <tr>
			    <td><strong>DATE OF INCIDENT:</strong></td>
			    <td colspan="2">'.$data["uir_date"].'</td>
			    <td><strong>TIME OF INCIDENT:</strong></td>
			    <td>'.$data["uir_time"].'</td>
			  </tr>
			  <tr>
			    <td><strong>RM/MIC ON DUTY:</strong></td>
			    <td colspan="2">'.$data["mic"].'</td>
			    <td><strong>NAME OF GUARDS:</strong></td>
			    <td>'.$data["sg"].'</td>
			  </tr>
			  <tr>
			    <td><strong>'.$label1.'</strong></td>
			    <td colspan="2">'.$value1.'</td>
			    <td><strong>'.$label2.'</strong></td>
			    <td>'.$value2.'</td>		  
			  </tr>
			  <tr>
			    <td colspan="4" bgcolor="#33CCFF"><strong>'.$label4.'</strong></td>
			    <td colspan="3" bgcolor="#33CCFF"><strong>'.$label5.'</strong></td>
			  </tr>
			  
			  <tr>
			    <td colspan="4">'.get_list_item($value4).'</td>
			    <td colspan="3">'.get_list_item($value5).'</td>
			  </tr>
			  <tr>
			    <td colspan="7"><strong>INITIAL ACTION TAKEN/DONE:</strong></td>
			  </tr>
			  <tr>
			    <td colspan="7">'.$data["initial_action"].'</td>
			  </tr>
			</table>';
			break;
			}
		if($resend)
			$pc = "PC# ";
		else
			$pc = "";
		return $layout;

	}
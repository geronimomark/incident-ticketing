<script type="text/javascript" src="<?=base_url()?>bootstrap/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?=base_url()?>bootstrap/assets/form-validation.js"></script>
<link href="<?=base_url()?>bootstrap/vendors/datepicker.css" rel="stylesheet" media="screen">
<script src="<?=base_url()?>bootstrap/vendors/bootstrap-datepicker.js"></script>
<link href="<?=base_url()?>bootstrap/assets/timepicker.css" rel="stylesheet" media="screen">
<script src="<?=base_url()?>bootstrap/assets/timepicker.js"></script>
<script>
    jQuery(document).ready(function() {   
       FormValidation.init();
    });
    $("#sort").click(function(){
    	var sort
    	var order
    	var filter
    	if($("#value_order").val() == ""){order = 0;}else{order = $("#value_order").val();}
    	if($("#value_sort").val() == ""){sort = 0;}else{sort = $("#value_sort").val();}
    	if($("#value_filter").val() == ""){filter = 0;}else{filter = $("#value_filter").val()+"-"+$("#value_filter_text").val();}
	        $.ajax({
	            type:       "GET",
	            url:        "<?php echo base_url(); ?>recipient_settings/get_email_list/"+order+"/"+sort+"/"+filter,
	            timeout:    1000,
	            success: function(data){
	                var obj = $.parseJSON(data);
	                if(obj.error != undefined) {
	                    alert(obj.error);
	                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);
	                    return;
	                }

	                var ctr = obj.length;
	                $("#recipient_list").empty();
	                for (var i = 0; i <= ctr-1; i++) {
	                    $("#recipient_list").append("<tr><td><span style='color:"+obj[i].color+"'>"+obj[i].color+ "</span></td><td>"+obj[i].email+"</td><td>"+obj[i].type+"</td><td><a data-toggle='modal' href='#myAlert2' data='"+obj[i].id+"' class='trash btn'><i class='icon-trash'></i></a></td></tr>");
	                }
	            }
	        });    
	});
	$("#recipient_list").on('click','.trash', function() {
		$("#delete").attr("data",$(this).attr("data"));
	});
	$("#delete").click(function(){
		var val = $(this).attr("data");
			$.ajax({
	            type:       "GET",
	            url:        "<?php echo base_url(); ?>recipient_settings/delete/"+val+"",
	            timeout:    1000,
	            success: function(data){
	                var obj = $.parseJSON(data);
	                if(obj.error != undefined) {
	                    alert(obj.error);
	                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);
	                    return;
	                }
	                 $("#sort").trigger("click");
	            }
	        }); 
	});
	$("#save").click(function() {
	        $("#submit").trigger("click");
	    });
 	$(function(){
 		 $("#sort").trigger("click");
 	});

 </script>
<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Recipient Settings</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
            <!-- BEGIN FORM-->
            <form class="form-horizontal" action="<?=base_url().'recipient_settings/save'?>" method="POST">
                <fieldset>
                    <?php if($this->session->flashdata("errorx")):?>
                    <div class="alert alert-error">
                        <button data-dismiss="alert" class="close"></button>
                        <?=$this->session->flashdata("errorx")?>
                    </div>
                    <?php endif ?>
                    <?php if($this->session->flashdata("successx")):?>
                    <div class="alert alert-success">
                        <button data-dismiss="alert" class="close"></button>
                        <?=$this->session->flashdata("successx")?>
                    </div>
                    <?php endif ?>
                    <div class="modal hide" id="myAlert" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Are you sure you want to save?</h3>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" id="save" data-dismiss="modal">Confirm</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                    <div class="modal hide" id="myAlert2" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Are you sure you want to delete?</h3>
                        </div>
                        <div class="modal-footer">
                            <a href="#" data="" class="btn btn-primary" id="delete" data-dismiss="modal">Confirm</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                        <!--Contents Here-->
                    <div class="control-group">
                        <label class="control-label">Security Code<span class="required">*</span></label>
                        <div class="controls">
                            <input type="hidden" class="span6 m-wrap" data-required="1" value="Security Code" id="label_secode" name="label_secode">
                            <select id="value_secode" name="value_secode" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <option value="RED">RED</option>
                                <option value="BLUE">BLUE</option>
                                <option value="ORANGE">ORANGE</option>
                                <option value="GREEN">GREEN</option>
                            </select>
                        </div>
                    </div>
                     <div class="control-group">
                        <label class="control-label">Type<span class="required">*</span></label>
                        <div class="controls">
                            <input type="hidden" class="span6 m-wrap" data-required="1" value="Security Code" id="label_type" name="label_type">
                            <select id="value_type" name="value_type" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <option value="1">Primary</option>
                                <option value="2">Secondary</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Email Add<span class="required">*</span></label>
                        <div class="controls">
                            <textarea class="span6 m-wrap" data-required="1" id="emailadd" name="emailadd" maxlength="500"></textarea>
                        </div>
                    </div>
                    <div class="form-actions">
                        <a class="btn btn-primary" data-toggle="modal" href="#myAlert" id="fake_submit">Submit</a>
                        <button class="btn btn-primary hidden" id="submit" type="submit"></button>
                        <button class="btn" type="button">Cancel</button>
                    </div>
                </fieldset>
            </form>
            <!-- END FORM-->
             </div>
        </div>
    </div>
               <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">List of Recipient</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
             <form class="form-horizontal" action="<?=base_url().'recipient_settings/search'?>" method="POST">
                <fieldset>
                   
                        <!--Contents Here-->
                    <label >Sort email list</label>
                    <div class="control-group">
                          <label class="control-label">Search by:</label>
                          <div class="controls">
                            <input type="hidden" class="span6 m-wrap" data-required="1" value="label_filter" id="label_filter" name="label_filter">
                            <select id="value_filter" name="value_filter" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <option value="color">COLOR</option>
                                <option value="email">EMAIL</option>
                                <option value="type">Type</option>
                                
                            </select>
                        </div>
                    </div>
                      <div class="control-group">
                          <label class="control-label">Keywords:</label>
                          <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="value_filter_text" name="value_filter_text">
                        </div>
                    </div>
                    <div class="control-group">
                          <label class="control-label">Sort By:</label>
                          <div class="controls">
                            <input type="hidden" class="span6 m-wrap" data-required="1" value="label_order" id="label_order" name="label_order">
                            <select id="value_order" name="value_order" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <option value="color">COLOR</option>
                                <option value="email">EMAIL</option>
                                <option value="type">Type</option>
                                <option value="4">COLOR and Type</option>
                            </select>
                            
                        </div>
                    </div>
                     <div class="control-group">
                        <label class="control-label">Order By:</label>
                        <div class="controls">
                            <input type="hidden" class="span6 m-wrap" data-required="1" value="ORDER" id="label_sort" name="label_sort">
                            <select id="value_sort" name="value_sort" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <option value="ASC">ASC</option>
                                <option value="DESC">DESC</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <a class="btn btn-primary" data-toggle="modal" href="#" id="sort">Sort</a>
                        <button class="btn btn-primary hidden" id="submit2" type="submit"></button>
                        
                    </div>
                </fieldset>
            </form>
              <div class="block-content collapse in">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Security Code</th>
                            <th>Email</th>
                            <th>Type</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody id="recipient_list"></tbody>
                </table>

            </div>
            </div>
        </div>
    </div>
    <!-- /block -->
</div>
<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Store Maintenance</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
            <!-- BEGIN FORM-->
            <form class="form-horizontal" action="<?=base_url()?>store/update" method="POST">
                <fieldset>
                    <div class="modal hide" id="myAlert" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Are you sure you want to update this store?</h3>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" id="save" data-dismiss="modal">Confirm</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Store Code:<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="store_code" name="store_code" readonly value="<?=$value[0]->store_code?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Name Code:<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="name_code" name="name_code" value="<?=$value[0]->name_code?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Store Name:<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="store_name" name="store_name" value="<?=$value[0]->store_name?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">PC Code:<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="pc_code" name="pc_code" value="<?=str_replace("PC#", "", $value[0]->pc_code)?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Store Type:<span class="required">*</span></label>
                        <div class="controls">
                            <select id="store_type" name="store_type" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <?php foreach($store_type as $k => $v):?>
                                <option value="<?=$v->store_type?>" <?php if($v->store_type == $value[0]->store_type):?>selected<?php endif ?>><?=$v->store_type?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button class="hide btn btn-primary" id="submit" type="submit">Search</button>
                        <a href="#myAlert" data-toggle="modal" class="btn btn-primary">Update</a>
                        <button class="btn" id="cancel" type="button">Cancel</button>
                    </div>
                </fieldset>
            </form> 
            <!-- END FORM-->
            </div>
        </div>
    </div>
    <!-- /block -->
</div>
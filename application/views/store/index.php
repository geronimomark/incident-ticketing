<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Add New Store Information</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
            <!-- BEGIN FORM-->
            <form class="form-horizontal" action="<?=base_url()?>store/save" method="POST">
                <fieldset>
                    <div class="modal hide" id="myAlert" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Are you sure you want to save this store?</h3>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" id="save" data-dismiss="modal">Confirm</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                     <div class="modal hide" id="myAlert2" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Are you sure you want to delete this strore?</h3>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" id="delete" data-dismiss="modal">Confirm</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Name Code:<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="name_code" name="name_code">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Store Name:<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="store_name" name="store_name">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">PC Code:<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="pc_code" name="pc_code">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Store Type:<span class="required">*</span></label>
                        <div class="controls">
                            <select id="store_type" name="store_type" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <?php foreach($store_type as $k => $value):?>
                                <option value="<?=$value->store_type?>"><?=$value->store_type?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button class="hide btn btn-primary" id="submit" type="submit">Search</button>
                        <a href="#myAlert" data-toggle="modal" class="btn btn-primary">Submit</a>
                         <a id="trigger_delete" href="#myAlert2" data-toggle="modal"  class="hide btn btn-inverse"></a>
                        <button class="btn" type="button">Cancel</button>
                    </div>
                </fieldset>
            </form> 
            <!-- END FORM-->
            </div>
        </div>
    </div>
    <!-- /block -->
</div>


<div class="row-fluid">
    <div class="span12">
        <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Sort Store Information</div>
        </div>
                <div class="block-content collapse in">
                    <div class="span12">
                        <form class="form-horizontal" action="<?=base_url()?>store/sort" method="POST">
                            <fieldset>
                        <div class="control-group">
                        <label class="control-label">Search By:<span class="required">*</span></label>
                        <div class="controls">
                            <select id="search" name="search" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <option value="store_code">Store Code</option>
                                 <option value="name_code">Name Code</option>
                                <option value="store_name">Store Name</option>
                                <option value="pc_code">PC#</option>
                                <option value="store_type">Store Type</option>
                           
                            </select>
                        </div>
                    </div>
                        <div class="control-group">
                        <label class="control-label">Keywords:<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="keyword" name="keyword" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button class="btn btn-primary" id="submit" type="submit">Search</button>
                        <button class="btn" type="button">Cancel</button>
                        <!-- to open modal -->
                       
                    </div>
                </form>
         </div>
     </fieldset>
        </div>
    </div>
        <div class="block">
            <div class="navbar navbar-inner block-header">
            <?php if(isset($search)): ?>
                <div class="muted pull-left"><?=$search?></div>
            <?php else: ?>
                <div class="muted pull-left">Store</div>
            <?php endif ?>
                <div class="pull-right"><span class="badge badge-info"><?=count($stores)?></span>

                </div>
           
            </div>

  
      

            <div class="block-content collapse in">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th> </th>
                            <th>Store Code</th>
                            <th>Name Code</th>
                            <th>Store Name</th>
                            <th>PC Code</th>
                            <th>Store Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($stores as $value): ?>
                        <tr>
                            <td>
                                <a data-toggle="modal" data="<?=$value->store_code?>" class="src btn"><i class="icon-edit"></i></a>
                                <a data-toggle="modal" data="<?=$value->store_code?>" class="rmv btn"><i class="icon-remove"></i></a>
                            </td>
                            <td><?=$value->store_code?></td>
                            <td><?=$value->name_code?></td>
                            <td><?=$value->store_name?></td>
                            <td><?=$value->pc_code?></td>
                            <td><?=$value->store_type?></td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
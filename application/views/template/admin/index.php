<!DOCTYPE html>
<html lang="en">
<head>
<?=$html_header?>
</head>
<body>
<?=$html_navbar?>
<div class="container-fluid">
    <div class="row-fluid">
        <?=$html_menu?>
		<div class="span9" id="content">
			<?php if($this->session->flashdata('error')):?>
			<div class="alert alert-error">
			<button class="close" data-dismiss="alert">&times;</button>
			<strong>Error!</strong> <?=$this->session->flashdata("error")?>
			</div>
			<?php endif ?>
			<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-success">
			<button class="close" data-dismiss="alert" type="button">&times;</button>
			<h4>Success</h4>
			<?=$this->session->flashdata('success')?>
			</div>
			<?php endif ?>
        	<?=$html_body?>
    	</div>
    </div>
    <?=$html_footer?>
</div>
<script src="<?=base_url();?>bootstrap/vendors/jquery-1.9.1.min.js"></script>
<script src="<?=base_url();?>bootstrap/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url();?>bootstrap/vendors/easypiechart/jquery.easy-pie-chart.js"></script>
<script src="<?=base_url();?>bootstrap/assets/scripts.js"></script>
<script src="<?=base_url()?>bootstrap/vendors/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>bootstrap/assets/DT_bootstrap.js"></script>
<?=$script?>
</body>
</html>
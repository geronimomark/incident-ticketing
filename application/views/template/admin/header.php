<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$title?></title>
<!-- Bootstrap -->
<link href="<?=base_url();?>bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="<?=base_url();?>bootstrap/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
<link href="<?=base_url();?>bootstrap/vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
<link href="<?=base_url();?>bootstrap/assets/styles.css" rel="stylesheet" media="screen">
<link href="<?=base_url()?>bootstrap/assets/DT_bootstrap.css" rel="stylesheet" media="screen">
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="<?=base_url();?>bootstrap/vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
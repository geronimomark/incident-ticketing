<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="<?=base_url()?>home"><?=COMPANY?></a>
            <div class="nav-collapse collapse">
                <ul class="nav pull-right">
                    <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?=$this->session->userdata("name")?> <i class="caret"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a tabindex="-1" href="#">Profile</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a tabindex="-1" href="<?=base_url()?>login/logout">Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
<!--                 <ul class="nav">
                    <li class="active">
                        <a href="<?=base_url()?>home">Dashboard</a>
                    </li>
                </ul> -->
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
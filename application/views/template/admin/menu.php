<div class="span3" id="sidebar">
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
        <li <?php if($active=="dashboard"):?>class="active"<?php endif;?>>
            <a href="<?=base_url()?>home"><i class="icon-home"></i> Dashboard</a>
        </li>
        <li <?php if($active=="uir_new"):?>class="active"<?php endif;?>>
            <a href="<?=base_url()?>uir_new"><i class="icon-pencil"></i> File Report</a>
        </li>
         <li <?php if($active=="recipient"):?>class="active"<?php endif;?>>
            <a href="<?=base_url()?>recipient_settings"><i class="icon-list-alt"></i> Recipient Settings</a>
        </li>
         <li class="dropdown <?php if($active=="maintenance"):?> active<?php endif;?>">
            <a class="dropdown-toggle" data-toggle="dropdown"><i class="icon-edit"></i> Maintenance</a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="<?=base_url()?>store"><i class="icon-shopping-cart"></i> Store</a></li>
                 <li><a href="<?=base_url()?>employee"><i class="icon-user"></i> Employee</a></li>
                 <li><a href="<?=base_url()?>incident"><i class="icon-fire"></i> Incident Info</a></li>
            </ul>
        </li>
        <li class="dropdown <?php if($active=='reports'):?> active<?php endif;?>">
            <a class="dropdown-toggle" data-toggle="dropdown"><i class="icon-th-list"></i> Reports</a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="<?=base_url()?>email"><i class="icon-envelope"></i> Search and E-Mail</a></li>
                <li><a href="<?=base_url()?>extract"><i class="icon-download"></i> Extract Data</a></li>
                <li><a href="<?=base_url()?>old_uir/search"><i class="icon-trash"></i> Archives</a></li>
               
            </ul>
        </li>
    </ul>
</div>
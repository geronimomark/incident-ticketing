<!DOCTYPE html>
<html lang="en">
<head>
	<title>LOGIN</title>
	<!-- Bootstrap -->
	<link href="<?=base_url();?>bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="<?=base_url();?>bootstrap/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
	<link href="<?=base_url();?>bootstrap/assets/styles.css" rel="stylesheet" media="screen">
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="<?=base_url();?>bootstrap/vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body id="login">
	<div class="container">
		<form class="form-signin" method="POST" action="<?=base_url()?>login/authenticate">
			<h2 class="form-signin-heading">Please sign in</h2>
			<?php if($this->session->flashdata('error')):?>
			<div class="alert alert-error">
			<button class="close" data-dismiss="alert">&times;</button>
			<strong>Login Error!</strong><br /><br /> <?=$this->session->flashdata("error")?>
			</div>
			<?php endif ?>
			<input type="text" name="username" class="input-block-level" placeholder="Username">
			<input type="password" name="password" class="input-block-level" placeholder="Password">
			<button class="btn btn-large btn-primary" type="submit">Sign in</button>
		</form>
	</div>
	<script src="<?=base_url();?>bootstrap/vendors/jquery-1.9.1.min.js"></script>
	<script src="<?=base_url();?>bootstrap/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
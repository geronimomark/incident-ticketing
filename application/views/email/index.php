<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Search and Email</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
            <!-- BEGIN FORM-->
            <form class="form-horizontal" action="" method="POST">
                <fieldset>
                    <div class="modal hide" id="myAlert" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Are you sure you want to resend this email?</h3>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" id="save" data-dismiss="modal">Confirm</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                    <div class="modal hide" id="myAlert2" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>View UIR</h3>
                        </div>
                        <div class="row-fluid" id='view'></div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" id="Update" data-dismiss="modal">Update</a>
                            <a href="#" class="btn btn-primary" id="resend" data-dismiss="modal">Resend</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                     <div class="modal hide" id="myAlert3" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Are you sure you want to delete this UIR?</h3>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" id="delete_yes" data-dismiss="modal">Confirm</a>
                            <a href="#" class="btn" data-dismiss="modal">No</a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Search By:<span class="required">*</span></label>
                        <div class="controls">
                            <select id="search" name="search" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <option value="A.uir_ticket_no">UIR Number</option>
                                <option value="main_name">UIR Class</option>
                                <option value="incident_name">Incident</option>
                                <option value="D.store_name">Store Name</option>
                                <option value="mic_name">MIC Name</option>
                                <option value="sg_name">SG Name</option>
                                <option value="uir_date">Incident Date</option>
                                <option value="A.last_update">Encode Date</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Keywords:<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="keyword" name="keyword">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button class="btn btn-primary" id="submit" type="submit">Search</button>
                        <button class="btn" type="button">Cancel</button>
                        <!-- to open modal -->
                        <a id="triggerview" href="#myAlert2" data-toggle="modal" class="hide btn btn-inverse"><i class="icon-envelope icon-white"></i></a>
                        <a id="trigger_delete" href="#myAlert3" data-toggle="modal" class="hide btn btn-inverse"><i class="icon-envelope icon-white"></i></a>
                    </div>
                </fieldset>
            </form>
            <!-- END FORM-->
            </div>
        </div>
    </div>
    <!-- /block -->
</div>
<?php if(isset($search)): ?>
<div class="row-fluid">
    <div class="span12">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left"><?=$search?></div>
                <div class="pull-right"><span class="badge badge-info"><?=count($data)?></span>

                </div>
            </div>
            <div class="block-content collapse in">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th> </th>
                            <th>Ticket No.</th>
                            <th>UIR Class</th>
                            <th>Incident</th>
                            <th>Store</th>
                            <th>MIC</th>
                            <th>SG</th>
                            <th>Encode Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $value): ?>
                        <tr>
                            <td>
                                <a data-toggle="modal" data="<?=$value->uir_ticket_no?>" class="search btn"><i class="icon-zoom-in"></i></a>
                                <a data-toggle="modal" data="<?=$value->uir_ticket_no?>" class="delete_uir btn btn-inverse"><i class="icon-trash icon-white"></i></a>
                            </td>
                            <td><?=$value->uir_ticket_no?></td>
                            <td><?=$value->main_name?></td>
                            <td><?=$value->incident_name?></td>
                            <td><?=$value->store_name?></td>
                            <td><?=$value->mic_name?></td>
                            <td><?=$value->sg_name?></td>
                            <td><?=$value->last_update?></td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<?php endif ?>
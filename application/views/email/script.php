<script>
	$(".delete_uir").click(function() {
       $("#delete_yes").attr("data",$(this).attr("data")) ;

		$("#trigger_delete").trigger("click");
	});
    $("#delete_yes").click(function() {
       
       setTimeout(window.location.replace("<?=base_url()?>email/delete_uir/"+$(this).attr("data")),1000);
    });
    $("#Update").click(function(){
        window.location.replace("<?=base_url()?>uir_new/edit/"+$(this).attr("data"));
    });

    $("#resend").click(function(e) {
        e.preventDefault();
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>email/resend/"+$(this).attr("data"),
            async:      false,
            // timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                var ctr = obj.length;
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                alert(obj.message);
            }
        });
    });

	$(".search").click(function() {
      		$.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>email/view/"+$(this).attr("data"),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                var ctr = obj.length;
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                $("#view").empty().css("padding", "30px");
                for (var i = 0; i <= ctr-1; i++) {
                	if(i === 0) {
                        $("#resend").attr("data", obj[i].uir_ticket_no);
                        $("#Update").attr("data", obj[i].uir_ticket_no);
                    	
                        $("#view").append("<table style='width:90%;font-size:12px' ><tr><td width='100'><b>Ticket No: </b></td><td>"+obj[i].uir_ticket_no+"</td>"+
                    					"<td><b>UIR Class: </b></td><td>"+obj[i].main_name+"</td></tr>"+
                    					"<tr><td><b>Incident: </b></td><td>"+obj[i].incident_name+"</td>"+
                    					"<td><b>Store: </b></td><td>"+obj[i].store_name+"</td></tr>"+
                                        "<tr><td><b>UIR Code: </b></td><td>"+obj[i].uir_code+"</td>"+
                                        "<td><b>Security Code: </b></td><td style='color: "+obj[i].security_code+"'><b>"+obj[i].security_code+"</b></td></tr>"+
                    					"<tr><td><b>MIC: </b></td><td>"+obj[i].mic_name+"</td>"+
                    					"<td><b>SG: </b></td><td>"+obj[i].sg_name+"</td></tr>"+
                    					"<tr><td><b>Date of Discovery: </b></td><td>"+obj[i].uir_date+"</td>"+
                    					"<td><b>Time of Discovery: </b></td><td>"+obj[i].uir_time+"</td></tr>"+
                    					"<tr><td><b>Location: </b></td><td>"+obj[i].location+"</td>"+
                    					"<td><b>Initial Action: </b></td><td>"+obj[i].initial_action+"</td></tr>"+
                    					"<tr><td><b>"+obj[i].label+": </b></td><td>"+obj[i].value+"</td>"+
                    					"<td height='30'><b>Encode Date: </b></td><td>"+obj[i].last_update+"</td></tr>"+
                    					"<tr><td><b>Summary: </b></td><td colspan='3'>"+obj[i].summary+"</td></tr>"

                    	);                       
                	} else {
                		$("#view").append("<tr><td>"+obj[i].label+": </td><td>"+obj[i].value+"</td></tr>");
                	}
                }
                $("#view").append("</table>");
            }
        });
        $("#triggerview").trigger("click");
	});
</script>
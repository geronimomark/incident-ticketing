<div class="row-fluid">
    <div class="span12">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">Old UIR</div>
                <div class="pull-right"><span class="badge badge-info"><?=$count?></span>

                </div>
            </div>
            <div class="block-content collapse in">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Ref Code</th>
                            <th>Store Code</th>
                            <th>Emp Code</th>
                            <th>Mic Code</th>
                            <th>SG Code</th>
                            <th>V Code</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records->result_object() as $key => $value):?>
                        <tr>
                            <td><?=$value->ref_code?></td>
                            <td><?=$value->store_code?></td>
                            <td><?=$value->emp_code?></td>
                            <td><?=$value->mic_code?></td>
                            <td><?=$value->sg_code?></td>
                            <td><?=$value->v_code?></td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /block -->
        <?=$pagination?>
    </div>
</div>
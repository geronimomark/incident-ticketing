<script type="text/javascript" src="<?=base_url()?>bootstrap/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?=base_url()?>bootstrap/assets/form-validation.js"></script>
<link href="<?=base_url()?>bootstrap/vendors/datepicker.css" rel="stylesheet" media="screen">
<script src="<?=base_url()?>bootstrap/vendors/bootstrap-datepicker.js"></script>
<link href="<?=base_url()?>bootstrap/assets/timepicker.css" rel="stylesheet" media="screen">
<script src="<?=base_url()?>bootstrap/assets/timepicker.js"></script>
<script>
    jQuery(document).ready(function() {   
       FormValidation.init();
    });

    $(".datepicker").datepicker({format: "yyyy-mm-dd"}).attr("readonly", "readonly");

    $('#time').timepicker();
    
    function clickme(x) {
        $("#rm_mic").val(x.innerHTML).attr("readonly", "readonly");
        $("#rm_mic_list").empty();
        $("#mica").removeClass("hidden");
    }

    function clickmesg(x) {
        $("#sg").val(x.innerHTML).attr("readonly", "readonly");
        $("#sg_list").empty();
        $("#sga").removeClass("hidden");
    }

    function clickmest(x) {
        $("#store").val(x.innerHTML).attr("readonly", "readonly");
        $("#store_list").empty();
        $("#sta").removeClass("hidden");
        $("#rm_mic").val("").removeAttr("readonly");
        $("#sg").val("").removeAttr("readonly");
    }

    $("#mica").click(function() {
        $("#rm_mic").val('').removeAttr("readonly");
        $("#mica").addClass("hidden");
    });

    $("#sga").click(function() {
        $("#sg").val('').removeAttr("readonly");
        $("#sga").addClass("hidden");
    });

    $("#sta").click(function() {
        $("#store").val('').removeAttr("readonly");
        $("#sta").addClass("hidden");
        $("#mica").trigger("click");
        $("#sga").trigger("click");
    });

    $("#uir_class").change(function() {
       load_incident($(this).val());
    });

    $("#incident").change(function() {
     load_security_code($(this).val());
    });

function load_incident(data){
       if(data != "") {
            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>uir_new/get_sub/"+data,
               // timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    var ctr = obj.length;
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    $("#incident").empty().append("<option value=''>---Select Incident---</option>");
                    for (var i = 0; i <= ctr-1; i++) {
                        $("#incident").append("<option data='"+obj[i].uir_code+"|"+obj[i].security_code+"' value='"+obj[i].incident_code+"'>"+obj[i].incident_name+"</option>");
                    }
                }
            });    
        } else {
            $("#incident").empty().append("<option value=''>---Select Incident---</option>").trigger("change");
        }
}
function load_security_code(data){
    if(data != "") {
    var data = $("#incident option:selected").attr("data");
    var x = data.split("|");
    $("#uir_code_value").val(x[0]);
    $("#sec_code_value").val(x[1]).css("background-color", x[1]);
    $("#uir_code").removeClass("hidden");
    $("#sec_code").removeClass("hidden");
    $.ajax({
        type:       "GET",
        url:        "<?php echo base_url(); ?>uir_new/get_details/"+x[0],
       // timeout:    1000,
        success: function(data){
            var obj = $.parseJSON(data);
            if(obj.error != undefined) {
                alert(obj.error);
                setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                return;
            }
            $("#details").empty().append(obj.data);
        }
    });
    } else {
        $("#uir_code").addClass("hidden");
        $("#sec_code").addClass("hidden").css("background-color", "white");
    }
}



    $("#rm_mic").keyup(function(e){
        $("#rm_mic_list").empty();
        if($(this).val() != "") {
            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>uir_new/get_mic/"+$(this).val()+"/"+$("#store").val(),
               // timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    var ctr = obj.length;
                    for (var i = 0; i <= ctr-1; i++) {
                        $("#rm_mic_list").append("<p class='help-block' onclick='clickme(this)' data='"+obj[i].mic_code+"'>"+obj[i].mic_code + '-' + obj[i].mic_name+"</p>");
                    }
                }
            });    
        }
    });

    $("#sg").keyup(function(e){
        $("#sg_list").empty();
        if($(this).val() != "") {
            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>uir_new/get_sg/"+$(this).val()+"/"+$("#store").val(),
             //   timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    var ctr = obj.length;
                    for (var i = 0; i <= ctr-1; i++) {
                        $("#sg_list").append("<p class='help-block' onclick='clickmesg(this)' data='"+obj[i].sg_code+"'>"+obj[i].sg_code + '-' + obj[i].sg_name+"</p>");
                    }
                }
            });    
        }
    });

    $("#store").keyup(function(e){
        $("#store_list").empty();
        if($(this).val() != "") {
            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>uir_new/get_store/"+$(this).val(),
           //     timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    var ctr = obj.length;
                    for (var i = 0; i <= ctr-1; i++) {
                        $("#store_list").append("<p class='help-block' onclick='clickmest(this)' data='"+obj[i].store_code+"'>"+obj[i].store_code + '-' + obj[i].store_name + '-' + obj[i].name_code + '-' + obj[i].pc_code +"</p>");
                    }
                }
            });    
        }
    });

    $("#save").click(function() {
        $("#submit").trigger("click");
    });
    $("#fake_submit2").click(function() {
       $("#action_form").attr("action","<?php echo base_url();?>uir_new/save_only");
    }); 

    $("#fake_submit").click(function() {
        if(update_form == false){
           $("#action_form").attr("action","<?php echo base_url();?>uir_new/save");
        }
      
    });

    $("#cancel").click(function() {
         if(window.location.href != "<?=base_url()?>uir_new"){
             window.location.replace("<?=base_url()?>email");
         }else{
         window.location.href = window.location.href;
         }
         
       
    });

//UPDATE UIR TICKE
var update_form = false;
$(window).load(function(){
var myurl = "<?php echo base_url();?>"+"uir_new/"+"<?php echo $this->uri->segment(2);?>";
if(myurl == "<?php echo base_url();?>uir_new/edit"){
    $("#triggerview").trigger("click");
    update_form = true;
}
});

$("#update").click(function(){  
     $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>email/view/"+"<?php echo $this->uri->segment(3);?>",
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                var ctr = obj.length;
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                //chenge action adress to save_update
                $("#action_form").attr("action","<?php echo base_url(); ?>uir_new/save_update/"+"<?php echo $this->uri->segment(3);?>");
                //hide the save button only update and cancel button show
                $("#fake_submit2").hide();
                

                for (var i = 0; i <= ctr-1; i++) {
                    if(i === 0) {
               //     alert(obj[i].incident_name);
                        $("#uir_class option").filter(function(){
                         return $(this).text() == ''+obj[i].main_name+'';
                       //    alert($(this).attr("data"));
                        }).prop("selected",true);
                        //load incident in option tag <optio></option>
                        load_incident(obj[i].main_code);
                        //fill text box 
                        $("#store").val(obj[i].store_name_code+'-'+obj[i].store_name+'-'+obj[i].pc_code);
                        $("#rm_mic").val(obj[i].mic_code+'-'+obj[i].mic_name);
                        $("#sg").val(obj[i].sg_code+'-'+obj[i].sg_name);
                        $("#date").val(obj[i].uir_date);
                        $("#time").val(obj[i].uir_time);
                        $("#location").val(obj[i].location);
                        $("#act").val(obj[i].initial_action);
                        $("#summary").val(obj[i].summary);
                        $("#email").val(obj[i].recipient);
                        //set variable
                        set_incident_name(obj[i].incident_name);
                        set_uir_no(obj[i].uir_ticket_no);
                        set_uir_code(obj[i].uir_code)
                        //call incident to fill incident name add time out to wait the UIR Class to load
                        setTimeout(select_incident,1000);
                        
                    } 

                }
            
            }
            //change form action location to save_update

        });
    

});
var incident_name;
var uir_no;
var uir_code;

function load_otherfields(){
   
     $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_otherfield/"+"<?php echo $this->uri->segment(3);?>",
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                var ctr = obj.length;
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var x = 0;
                for (var i = 0; i <= ctr-1; i++) {
                 
                  x = i + 1;
               //       
                       
                        //get inputboox name
                        
                        //get type of input box
                       if ($("#value_"+uir_code+x).prop("tagName") == "SELECT"){
                    
                        $("#value_"+uir_code+ x + " option").filter(function(){
                            return $(this).text() == obj[i].value;
                            }).prop("selected",true);

                      //   alert(obj[i].value);
                       
                           }  else{
                          
                            //fill text box
                         
                            $("#value_"+uir_code+x).val(obj[i].value); 

                        }
                   
                    

                }
            
            }

    });
}

function select_incident(){
    $("#incident option").filter(function(){
    return $(this).text() == ''+incident_name+'';
    }).prop("selected",true);
//$("incident").trigger("change");
load_security_code("x");//x is only a value to avoid null value dont be confuse with x and dont take it to serious :)
setTimeout(load_otherfields,1000);


}

function set_incident_name(i){
   incident_name = i;
}
function set_uir_no(i){
    uir_no = i;
}
function set_uir_code(i){
    uir_code = i.toLowerCase();
}

</script>
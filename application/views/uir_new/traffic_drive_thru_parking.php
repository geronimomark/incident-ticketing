<div class="control-group">
    <label class="control-label">Name Involved Vehicle1<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name Involved Vehicle1" id="label_tdp1" name="label_tdp1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_tdp1" name="value_tdp1"> <a id="tdpa" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="tdp_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Name Involved Vehicle2<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name Involved Vehicle2" id="label_tdp2" name="label_tdp2">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_tdp2" name="value_tdp2"> <a id="tdpa" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="tdp_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
        <label class="control-label">Injuries<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Injuries" id="label_tdp3" name="label_tdp3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_tdp3" name="value_tdp3"> <a id="tdpa" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="tdp_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Damage<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Damage" id="label_tdp4" name="label_tdp4">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_tdp4" name="value_tdp4">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_tdp1").val(x.innerHTML).attr("readonly", "readonly");
    $("#tdp_list").empty();
    $("#tdpa").removeClass("hidden");
}
$("#tdpa").click(function() {
    $("#value_tdp1").val('').removeAttr("readonly");
    $("#tdpa").addClass("hidden");
});
$("#value_tdp1").keyup(function(e){
    $("#tdp_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#tdp_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
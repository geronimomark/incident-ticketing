<div class="control-group">
    <label class="control-label">Name of Victim/Rider<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name of Victim/Rider" id="label_ra1" name="label_ra1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_ra1" name="value_ra1"> <a id="raa" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="ra_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
    <label class="control-label">With Companion<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="With Companion" id="label_ra2" name="label_ra2">
        <select id="value_ra2" name="value_ra2" class="span6 m-wrap">
             <option value="">---Select---</option>
            <option value="NO">NO</option>
            <option value="YES">YES</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Injured Part</label>
</div>
<div class="control-group">
    <label class="control-label">Head<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="head" id="label_ra3" name="label_ra3">
        <select id="value_ra3" name="value_ra3" class="span6 m-wrap">
            <option value="NO">NO</option>
            <option value="YES">YES</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Neck<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="neck" id="label_ra4" name="label_ra4">
        <select id="value_ra4" name="value_ra4" class="span6 m-wrap">
            <option value="NO">NO</option>
            <option value="YES">YES</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Spine/arm/leg<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Spine/arm/leg" id="label_ra5" name="label_ra5">
        <select id="value_ra5" name="value_ra5" class="span6 m-wrap">
            <option value="NO">NO</option>
            <option value="YES">YES</option>
            
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Name of hospital</label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name of hospital" id="label_ra6" name="label_ra6">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_ra6" name="value_ra6">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_ra1").val(x.innerHTML).attr("readonly", "readonly");
    $("#ra_list").empty();
    $("#raa").removeClass("hidden");
}
$("#raa").click(function() {
    $("#value_ra1").val('').removeAttr("readonly");
    $("#raa").addClass("hidden");
});
$("#value_ra1").keyup(function(e){
    $("#ra_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#ra_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
<div class="control-group">
    <label class="control-label">Name of Detected the Fire<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name of Detected the Fire" id="label_f1" name="label_f1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_f1" name="value_f1"> <a id="fa" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="f_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Prevent the Fire<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Prevent the Fire" id="label_f2" name="label_f2">
        <select id="value_f2" name="value_f2" class="span6 m-wrap">
             <option value="">---Select---</option>
            <option value="NO">NO</option>
            <option value="YES">YES</option>
        </select>
    </div>
</div>

<div class="control-group">
    <label class="control-label">Type / Cause of fire</label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Type / Cause of fire" id="label_f3" name="label_f3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_f3" name="value_f3">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_f1").val(x.innerHTML).attr("readonly", "readonly");
    $("#f_list").empty();
    $("#fa").removeClass("hidden");
}
$("#fa").click(function() {
    $("#value_f1").val('').removeAttr("readonly");
    $("#fa").addClass("hidden");
});
$("#value_f1").keyup(function(e){
    $("#f_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#f_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
<div class="control-group">
    <label class="control-label">Name Involved<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name Involved" id="label_lfo1" name="label_lfo1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_lfo1" name="value_lfo1"> <a id="lfoa" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="lfo_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
        <label class="control-label">No. of Offense<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="No. of Offense" id="label_lfo2" name="label_lfo2">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_lfo2" name="value_lfo2">
    </div>
</div>
<div class="control-group">
        <label class="control-label">No. of Transactions<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="No. of Transactions" id="label_lfo3" name="label_lfo3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_lfo3" name="value_lfo3">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Food Ordered<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Food Ordered" id="label_lfo4" name="label_lfo4">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_lfo4" name="value_lfo4">
    </div>
</div>
<div class="control-group">
    <label class="control-label">POS No.<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="POS No." id="label_lfo5" name="label_lfo5">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_lfo5" name="value_lfo5">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_lfo1").val(x.innerHTML).attr("readonly", "readonly");
    $("#lfo_list").empty();
    $("#lfoa").removeClass("hidden");
}
$("#lfoa").click(function() {
    $("#value_lfo1").val('').removeAttr("readonly");
    $("#lfoa").addClass("hidden");
});
$("#value_lfo1").keyup(function(e){
    $("#lfo_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#lfo_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
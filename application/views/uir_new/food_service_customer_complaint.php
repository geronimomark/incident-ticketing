<div class="control-group">
    <label class="control-label">Name Involved<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name Involved" id="label_fsc-fscc1" name="label_fsc-fscc1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_fsc-fscc1" name="value_fsc-fscc1"> <a id="fsc-fscca" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="fsc-fscc_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
        <label class="control-label">No. of Offense<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="No. of Offense" id="label_fsc-fscc2" name="label_fsc-fscc2">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_fsc-fscc2" name="value_fsc-fscc2"> <a id="fsc-fscca" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="fsc-fscc_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
    <label class="control-label">No. of Transaction<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="No. of Transaction" id="label_fscc3" name="label_fsc-fscc3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_fsc-fscc3" name="value_fsc-fscc3">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Food Ordered<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Food Ordered" id="label_fsc-fscc4" name="label_fsc-fscc4">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_fsc-fscc4" name="value_fsc-fscc4">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_fsc-fscc1").val(x.innerHTML).attr("readonly", "readonly");
    $("#fsc-fscc_list").empty();
    $("#fsc-fscca").removeClass("hidden");
}
$("#fsc-fscca").click(function() {
    $("#value_fsc-fscc1").val('').removeAttr("readonly");
    $("#fsc-fscca").addClass("hidden");
});
$("#value_fsc-fscc1").keyup(function(e){
    $("#fsc-fscc_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#fsc-fscc_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
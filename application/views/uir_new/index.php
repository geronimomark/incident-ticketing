﻿<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><?=$form_type?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
            <!-- BEGIN FORM-->
            <form class="form-horizontal" id="action_form" action="<?=base_url().'uir_new/save'?>" method="POST">
                <fieldset>
                    <?php if($this->session->flashdata("errorx")):?>
                    <div class="alert alert-error">
                        <button data-dismiss="alert" class="close"></button>
                        <?=$this->session->flashdata("errorx")?>
                    </div>
                    <?php endif ?>
                    <?php if($this->session->flashdata("successx")):?>
                    <div class="alert alert-success">
                        <button data-dismiss="alert" class="close"></button>
                        <?=$this->session->flashdata("successx")?>
                    </div>
                    <?php endif ?>
                    <div class="modal hide" id="myAlert" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Are you sure you want to save?</h3>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" id="save" data-dismiss="modal">Confirm</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                    <div class="modal hide" id="myAlert2" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Continue Update?</h3>
                        </div>
                        <div class="row-fluid" id='view'></div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" id="update" data-dismiss="modal">Continue</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                     <a id="triggerview" href="#myAlert2" data-toggle="modal" class="hide btn btn-inverse"></a>
                      <div class="control-group">
                        <label class="control-label">UIR Class<span class="required">*</span></label>
                        <div class="controls">
                            <select id="uir_class" name="uir_class" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <?php foreach ($uir_class as $key => $value):?>
                                <option value="<?=$value->main_code?>"><?=$value->main_name?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">

                        <label class="control-label">Incident<span class="required">*</span></label>
                        <div class="controls">
                            <select id="incident" name="incident" class="span6 m-wrap">
                            </select>
                        </div>
                    </div>
                    <div id="uir_code" class="control-group hidden">
                        <label class="control-label">UIR Code</label>
                        <div class="controls">
                            <input id="uir_code_value" name="uir_code_value" readonly type="text" class="span6 m-wrap" data-required="1">
                        </div>
                    </div>
                    <div id="sec_code" class="control-group hidden">
                        <label class="control-label">Security Code</label>
                        <div class="controls">
                            <input id="sec_code_value" name="sec_code_value" readonly type="text" class="span6 m-wrap" data-required="1">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Store<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="store" name="store" autocomplete="off"> <a id="sta" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
                            <div id="store_list" class="help-block"></div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">RM/MIC<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="rm_mic" name="rm_mic" autocomplete="off"> <a id="mica" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
                            <div id="rm_mic_list" class="help-block"></div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">SG<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="sg" name="sg" autocomplete="off"> <a id="sga" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
                            <div id="sg_list" class="help-block"></div>
                        </div>
                    </div>
                    <div id="details"></div>
                    <div class="control-group">
                        <label for="date" class="control-label">Date of Discovery<span class="required">*</span></label>
                        <div class="controls">
                        <input type="text" value="" id="date" name="date" class="input-xlarge datepicker">
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="time" class="control-label">Time of Discovery<span class="required">*</span></label>
                        <div class="controls bootstrap-timepicker">
                        <input id="time" name="time" type="text" class="input-small"> <span class="add-on"><i class="icon-time"></i></span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Incident Location<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="location" name="location">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Initial Action<span class="required">*</span></label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="act" name="act" autocomplete="off">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Summary</label>
                        <div class="controls">
                            <textarea class="span6 m-wrap" id="summary" name="summary"></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Email</label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" id="email" name="email">
                            <label style="font-size:12px;color:#999;font-family:Tahoma, Geneva, sans-serif" name="test" type="text" ><u>abc1@mail.com, abc2@mail.com</u></label>
                        </div>
                    </div>
                    <div class="form-actions">
                        <a class="btn btn-primary" data-toggle="modal" href="#myAlert" id="fake_submit"><?=$button_name?></a>
                        <a class="btn btn-primary" data-toggle="modal" href="#myAlert" id="fake_submit2">Save</a>
                        <button class="btn btn-primary hidden" id="submit" type="submit"></button>
                        <button class="btn" id="cancel" type="button">Cancel</button>
                    </div>
                </fieldset>
            </form>
            <!-- END FORM-->
            </div>
        </div>
    </div>
    <!-- /block -->
</div>
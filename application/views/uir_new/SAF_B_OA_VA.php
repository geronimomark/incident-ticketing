<div class="control-group">
    <label class="control-label">Name of Victim/Patient<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name of Victim/Patient" id="label_<?=$val?>1" name="label_<?=$val?>1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_<?=$val?>1" name="value_<?=$val?>1"> <a id="<?=$val?>a" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="<?=$val?>_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
    <label class="control-label">With Companion<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="With Companion" id="label_<?=$val?>2" name="label_<?=$val?>2">
        <select id="value_<?=$val?>2" name="value_<?=$val?>2" class="span6 m-wrap">
             <option value="">---Select---</option>
            <option value="NO">NO</option>
            <option value="YES">YES</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Injured Part</label>
</div>
<div class="control-group">
    <label class="control-label">Head<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Head" id="label_<?=$val?>3" name="label_<?=$val?>3">
        <select id="value_<?=$val?>3" name="value_<?=$val?>3" class="span6 m-wrap">
            <option value="NO">NO</option>
            <option value="YES">YES</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Neck<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Neck" id="label_<?=$val?>4" name="label_<?=$val?>4">
        <select id="value_<?=$val?>4" name="value_<?=$val?>4" class="span6 m-wrap">
            <option value="NO">NO</option>
            <option value="YES">YES</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Spine/arm/leg<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Spine/arm/leg" id="label_<?=$val?>5" name="label_<?=$val?>5">
        <select id="value_<?=$val?>5" name="value_<?=$val?>5" class="span6 m-wrap">
            <option value="NO">NO</option>
            <option value="YES">YES</option>
            
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Name of hospital</label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name of hospital" id="label_<?=$val?>6" name="label_<?=$val?>6">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_<?=$val?>6" name="value_<?=$val?>6">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_<?=$val?>1").val(x.innerHTML).attr("readonly", "readonly");
    $("#<?=$val?>_list").empty();
    $("#<?=$val?>a").removeClass("hidden");
}
$("#<?=$val?>a").click(function() {
    $("#value_<?=$val?>1").val('').removeAttr("readonly");
    $("#<?=$val?>a").addClass("hidden");
});
$("#value_<?=$val?>1").keyup(function(e){
    $("#<?=$val?>_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#<?=$val?>_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
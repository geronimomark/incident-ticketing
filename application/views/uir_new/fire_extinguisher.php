<div class="control-group">
    <label class="control-label">Supplier Name<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Supplier Name" id="label_fe1" name="label_fe1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_fe1" name="value_fe1">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Date Last Repair<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Date Last Repair" id="label_fe2" name="label_fe2">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_fe2" name="value_fe2">
    </div>
</div>
</div>
<div class="control-group">
    <label class="control-label">Sched of Ref.<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Sched of Ref." id="label_fe3" name="label_fe3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_fe3" name="value_fe3">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Specific Location<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Specific Location" id="label_fe4" name="label_fe4">
        <select id="value_fe4" name="value_fe4" class="span6 m-wrap">
            <option value="">---Select---</option>
            <option value="Expired">Expired</option>
            <option value="Low Pressure">Low Pressure</option>
            <option value="Damage">Damage</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Type of Defect<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Type of Defect" id="label_fe5" name="label_fe5">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_fe5" name="value_fe5">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_fe1").val(x.innerHTML).attr("readonly", "readonly");
    $("#fe_list").empty();
    $("#fea").removeClass("hidden");
}
$("#fea").click(function() {
    $("#value_fe1").val('').removeAttr("readonly");
    $("#fea").addClass("hidden");
});
$("#value_fe1").keyup(function(e){
    $("#fe_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#fe_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
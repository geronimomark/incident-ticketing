<div class="control-group">
    <label class="control-label">Name Involved<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name Involved" id="label_uv1" name="label_uv1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_uv1" name="value_uv1"> <a id="uva" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="uv_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
        <label class="control-label">No. of Offense<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="No. of Offense" id="label_uv2" name="label_uv2">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_uv2" name="value_uv2">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Uniform Violated/ Not Worn<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Uniform Violated/ Not Worn" id="label_uv3" name="label_uv3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_uv3" name="value_uv3">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_uv1").val(x.innerHTML).attr("readonly", "readonly");
    $("#uv_list").empty();
    $("#uva").removeClass("hidden");
}
$("#uva").click(function() {
    $("#value_uv1").val('').removeAttr("readonly");
    $("#uva").addClass("hidden");
});
$("#value_uv1").keyup(function(e){
    $("#uv_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#uv_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
<div class="control-group">
    <label class="control-label">Name Involved<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name Involved" id="label_emv1" name="label_emv1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_emv1" name="value_emv1"> <a id="emva" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="emv_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
        <label class="control-label">No. of Offense<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="No. of Offense" id="label_emv2" name="label_emv2">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_emv2" name="value_emv2"> <a id="emva" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="emv_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Food Punched<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Food Punched" id="label_emv3" name="label_emv3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_emv3" name="value_emv3">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_emv1").val(x.innerHTML).attr("readonly", "readonly");
    $("#emv_list").empty();
    $("#emva").removeClass("hidden");
}
$("#emva").click(function() {
    $("#value_emv1").val('').removeAttr("readonly");
    $("#emva").addClass("hidden");
});
$("#value_emv1").keyup(function(e){
    $("#emv_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#emv_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
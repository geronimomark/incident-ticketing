<div class="control-group">
    <label class="control-label">Name Involved<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name Involved" id="label_t1" name="label_t1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_t1" name="value_t1"> <a id="ta" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="t_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
        <label class="control-label">No. of Offense<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="No. of Offense" id="label_t2" name="label_t2">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_t2" name="value_t2">
    </div>
</div>
<div class="control-group">
        <label class="control-label">Crew Sched<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Crew Sched" id="label_t3" name="label_t3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_t3" name="value_t3">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Minutes Late<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Minutes Late" id="label_t4" name="label_t4">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_t4" name="value_t4">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_t1").val(x.innerHTML).attr("readonly", "readonly");
    $("#t_list").empty();
    $("#ta").removeClass("hidden");
}
$("#ta").click(function() {
    $("#value_t1").val('').removeAttr("readonly");
    $("#ta").addClass("hidden");
});
$("#value_t1").keyup(function(e){
    $("#t_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#t_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
<div class="control-group">
    <label class="control-label">Name of Crew involved<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name of Crew involved" id="label_et1" name="label_et1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_et1" name="value_et1"> <a id="eta" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="et_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Length of Tenure<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Length of Tenure" id="label_et2" name="label_et2">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_et2" name="value_et2">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Alleged Item/Cash Taken<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Alleged Item/Cash Taken" id="label_et3" name="label_et3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_et3" name="value_et3">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_et1").val(x.innerHTML).attr("readonly", "readonly");
    $("#et_list").empty();
    $("#eta").removeClass("hidden");
}
$("#eta").click(function() {
    $("#value_et1").val('').removeAttr("readonly");
    $("#eta").addClass("hidden");
});
$("#value_et1").keyup(function(e){
    $("#et_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#et_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
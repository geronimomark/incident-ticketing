<div class="control-group">
    <label class="control-label">Name Involved<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name Involved" id="label_nit1" name="label_nit1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_nit1" name="value_nit1"> <a id="nita" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="nit_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
        <label class="control-label">No. of Offense<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="No. of Offense" id="label_nit2" name="label_nit2">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_nit2" name="value_nit2">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Reason<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Reason" id="label_nit3" name="label_nit3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_nit3" name="value_nit3">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_nit1").val(x.innerHTML).attr("readonly", "readonly");
    $("#nit_list").empty();
    $("#nita").removeClass("hidden");
}
$("#nita").click(function() {
    $("#value_nit1").val('').removeAttr("readonly");
    $("#nita").addClass("hidden");
});
$("#value_nit1").keyup(function(e){
    $("#nit_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#nit_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
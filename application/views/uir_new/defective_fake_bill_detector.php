<div class="control-group">
    <label class="control-label">Supplier Name<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Supplier Name" id="label_fbd1" name="label_fbd1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_fbd1" name="value_fbd1">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Date Last Repair<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Date Last Repair" id="label_fbd2" name="label_fbd2">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_fbd2" name="value_fbd2">
    </div>
</div>
</div>
<div class="control-group">
    <label class="control-label">Last PM/ Replacement<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Last PM/ Replacement" id="label_fbd3" name="label_fbd3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_fbd3" name="value_fbd3">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Type of Defect<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Type of Defect" id="label_fbd4" name="label_fbd4">
        <select id="value_fbd4" name="value_fbd4" class="span6 m-wrap">
            <option value="">---Select---</option>
            <option value="Busted UV Bulb">Busted UV Bulb</option>
            <option value="Defective Mechanism">Defective Mechanism</option>
            <option value="Damage">Damage</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Counter Location<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Counter Location" id="label_fbd5" name="label_fbd5">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_fbd5" name="value_fbd5">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_fbd1").val(x.innerHTML).attr("readonly", "readonly");
    $("#fbd_list").empty();
    $("#fbda").removeClass("hidden");
}
$("#fbda").click(function() {
    $("#value_fbd1").val('').removeAttr("readonly");
    $("#fbda").addClass("hidden");
});
$("#value_fbd1").keyup(function(e){
    $("#fbd_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#fbd_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
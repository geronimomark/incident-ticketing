<div class="control-group">
    <label class="control-label">Name of Involved<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name of Involved" id="label_<?=$val?>1" name="label_<?=$val?>1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_<?=$val?>1" name="value_<?=$val?>1">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Remarks<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Remarks" id="label_<?=$val?>2" name="label_<?=$val?>2">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_<?=$val?>2" name="value_<?=$val?>2">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_<?=$val?>1").val(x.innerHTML).attr("readonly", "readonly");
    $("#<?=$val?>_list").empty();
    $("#<?=$val?>a").removeClass("hidden");
}
$("#<?=$val?>a").click(function() {
    $("#value_<?=$val?>1").val('').removeAttr("readonly");
    $("#<?=$val?>a").addClass("hidden");
});
$("#value_<?=$val?>1").keyup(function(e){
    $("#<?=$val?>_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#<?=$val?>_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
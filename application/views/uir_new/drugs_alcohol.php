<div class="control-group">
    <label class="control-label">Name of Suspect<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name of Suspect" id="label_DAI-D-AI1" name="label_DAI-D-AI1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_DAI-D-AI1" name="value_DAI-D-AI1">
    </div>
</div>
<div class="control-group">
    <label class="control-label">CCTV focus/capture<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="CCTV focus/capture" id="label_DAI-D-AI2" name="label_DAI-D-AI2">
        <select id="value_DAI-D-AI2" name="value_DAI-D-AI2" class="span6 m-wrap">
            <option value="">---Select---</option>
            <option value="YES">YES</option>
            <option value="NO">NO</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Suspect's Drug and Alcohol Indicators<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Suspect's Drug and Alcohol Indicators" id="label_DAI-D-AI3" name="label_DAI-D-AI3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_DAI-D-AI3" name="value_DAI-D-AI3">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_DAI-D-AI1").val(x.innerHTML).attr("readonly", "readonly");
    $("#DAI-D-AI_list").empty();
    $("#DAI-D-AIa").removeClass("hidden");
}
$("#DAI-D-AIa").click(function() {
    $("#value_DAI-D-AI1").val('').removeAttr("readonly");
    $("#DAI-D-AIa").addClass("hidden");
});
$("#value_DAI-D-AI1").keyup(function(e){
    $("#DAI-D-AI_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#DAI-D-AI_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
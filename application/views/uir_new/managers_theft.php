<div class="control-group">
    <label class="control-label">Manager Involved<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Manager Involved" id="label_mt1" name="label_mt1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_mt1" name="value_mt1"> <a id="mta" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="mt_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Position<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Position" id="label_mt2" name="label_mt2">
        <select id="value_mt2" name="value_mt2" class="span6 m-wrap">
            <option value="">---Select---</option>
            <option value="MT">MT</option>
            <option value="1st Assistant">1st Assistant</option>
            <option value="2nd Assistant">2nd Assistant</option>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Alleged Item/Cash Taken<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Alleged Item/Cash Taken" id="label_mt3" name="label_mt3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_mt3" name="value_mt3">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_mt1").val(x.innerHTML).attr("readonly", "readonly");
    $("#mt_list").empty();
    $("#mta").removeClass("hidden");
}
$("#mta").click(function() {
    $("#value_mt1").val('').removeAttr("readonly");
    $("#mta").addClass("hidden");
});
$("#value_mt1").keyup(function(e){
    $("#mt_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#mt_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
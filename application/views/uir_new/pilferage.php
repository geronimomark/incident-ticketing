<div class="control-group">
    <label class="control-label">Name Involved<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name Involved" id="label_pil1" name="label_pil1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_pil1" name="value_pil1"> <a id="pila" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="pil_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
        <label class="control-label">No. of Offense<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="No. of Offense" id="label_pil2" name="label_pil2">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_pil2" name="value_pil2">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Items Taken<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Items Taken" id="label_pil3" name="label_pil3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_pil3" name="value_pil3">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_pil1").val(x.innerHTML).attr("readonly", "readonly");
    $("#pil_list").empty();
    $("#pila").removeClass("hidden");
}
$("#pila").click(function() {
    $("#value_pil1").val('').removeAttr("readonly");
    $("#pila").addClass("hidden");
});
$("#value_pil1").keyup(function(e){
    $("#pil_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#pil_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
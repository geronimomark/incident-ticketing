<div class="control-group">
    <label class="control-label">Name Involved<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name Involved" id="label_laf1" name="label_laf1">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_laf1" name="value_laf1"> <a id="lafa" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
        <div id="laf_list" class="help-block"></div>
    </div>
</div>
<div class="control-group">
        <label class="control-label">No. of Offense<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="No. of Offense" id="label_laf2" name="label_laf2">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_laf2" name="value_laf2">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Name of Owner<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Name of Owner" id="label_laf3" name="label_laf3">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_laf3" name="value_laf3">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Left Item<span class="required">*</span></label>
    <div class="controls">
        <input type="hidden" class="span6 m-wrap" data-required="1" value="Left Item" id="label_laf4" name="label_laf4">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_laf4" name="value_laf4">
    </div>
</div>
<script>
function clickmemt(x) {
    $("#value_laf1").val(x.innerHTML).attr("readonly", "readonly");
    $("#laf_list").empty();
    $("#lafa").removeClass("hidden");
}
$("#lafa").click(function() {
    $("#value_laf1").val('').removeAttr("readonly");
    $("#lafa").addClass("hidden");
});
$("#value_laf1").keyup(function(e){
    $("#laf_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>uir_new/get_employee/"+$(this).val()+"/"+$("#store").val(),
            timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                    $("#laf_list").append("<p class='help-block' onclick='clickmemt(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                }
            }
        });    
    }
});
</script>
<script type="text/javascript" src="<?=base_url()?>bootstrap/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?=base_url()?>bootstrap/assets/form-validation.js"></script>
<link href="<?=base_url()?>bootstrap/vendors/datepicker.css" rel="stylesheet" media="screen">
<script src="<?=base_url()?>bootstrap/vendors/bootstrap-datepicker.js"></script>
<link href="<?=base_url()?>bootstrap/assets/timepicker.css" rel="stylesheet" media="screen">
<script src="<?=base_url()?>bootstrap/assets/timepicker.js"></script>
<script>
    jQuery(document).ready(function() {   
       FormValidation.init();
    });
 function clickmestore(x) {
    $("#value_store").val(x.innerHTML).attr("readonly", "readonly");
    $("#store_list").empty();
    $("#storea").removeClass("hidden");
}
$("#storea").click(function() {
    $("#value_store").val('').removeAttr("readonly");
    $("#storea").addClass("hidden");
});
$("#value_store").keyup(function(e){
    $("#store_list").empty();
    if($(this).val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>employee/get_store/"+$(this).val(),
           // timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                for (var i = 0; i <= ctr-1; i++) {
                	  $("#store_list").append("<p class='help-block' onclick='clickmestore(this)' data='"+obj[i].store_code+"'>"+obj[i].store_code + '-' + obj[i].store_name + '-' + obj[i].name_code + '-' + obj[i].pc_code +"</p>");
                 }
            }
        });    
    }
});
$("#save").click(function() {
	        $("#update").trigger("click");
	    });
function getEmployeeType(type)
{
	var my_value = "";
	if (type == 0){
		my_value = "get_employee";
	}else if(type == 1){
		my_value = "get_mic";
	}else if(type == 2){
		my_value = "get_sg";
	}
	return my_value;
};
 $("#value_action").change(function() {
        if($(this).val() != "" ) {
           var val = $(this).val();
            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>employee/get_details/"+val,
              //  timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    $("#details").empty().append(obj.data);
                    if($("#value_action").val() == "0"){
                    	 $("#fake_submit").empty().append("SAVE");
                    	 $("#form_save").attr("action",'<?php echo base_url();?>employee/save');
                    }else if($("#value_action").val() == "1")
                    {
                    	$("#fake_submit").empty().append("UPDATE");
                    	$("#form_save").attr("action",'<?php echo base_url();?>employee/transfer_emp');
                    }
                }
            });
        }
    });
$(".remove").click(function(){
    $("#delete").attr("data",$(this).attr("data"));
});
$("#delete").click(function(){
    var val = $("#search_type").val();
            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>employee/delete/"+val+"/"+$(this).attr("data"),
              //  timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    location.reload();
                }
            });
});
    $(".edit").click(function(){
     
    $("#edit").attr("data",$(this).attr("data"));
    var empcode = $(this).attr("data");
    var val = $("#search_type").val();
    var item;
    if (val == 0){
        item = ["emp_fname","emp_sname","emp_initial","emp_shortname","emp_code"];
    }else if(val == 1){
         item = ["mic_fname","mic_sname","mic_initial","mic_shortname","mi_code"];
    }else if(val == 2){
        item = ["sg_fname","sg_sname","sg_initial","sg_shortname","sg_code"];
    }

            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>employee/get_emp_info/"+val+"/"+$(this).attr("data"),
           //     timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    $("#view").empty();
                    $("#view").append("<div class='control-group'>"+
                    "<input type='hidden' class='span2 m-wrap' data-required='1' id='emp_type' name='emp_type' value='"+val+"'>"+
                    "<input type='hidden' class='span2 m-wrap' data-required='1' id='emp_code' name='emp_code' value='"+empcode+"'>"+
                    "<label class='control-label'>Firt Name:<span class='required'>*</span></label>"+
                    "<div class='controls'>"+
                    "<input type='text' class='span6 m-wrap' data-required='1' id='update_fname' name='update_fname' value='"+obj[0][item[0]]+"' autocomplete='off'>"+
                    "</div>"+
                    "</div>"+
                    "<div class='control-group'>"+
                    "<label class='control-label'>Last Name:</label>"+
                    "<div class='controls'>"+
                    "<input type='text' class='span6 m-wrap' data-required='1' id='update_lname' name='update_lname' value='"+obj[0][item[1]]+"' autocomplete='off'>"+
                    "</div>"+
                    "</div>"+
                    "<div class='control-group'>"+
                    "<label class='control-label'>Middle Initial:</label>"+
                    "<div class='controls'>"+
                    "<input type='text' class='span2 m-wrap' data-required='1' id='update_mi' name='update_mi' value='"+obj[0][item[2]]+"' autocomplete='off'>"+
                    "</div>"+
                    "</div>"+
                    "<div class='control-group'>"+
                    "<label class='control-label'>Short Name:</label>"+
                    "<div class='controls'>"+
                    "<input type='text' class='span6 m-wrap' id='update_sname' name='update_sname' value='"+obj[0][item[3]]+"' autocomplete='off'>"+
                    "</div>"+
                    "</div>");
                }
            });

});
$("#edit").click(function(){
  $("#edit_emp").trigger("click")
});

 </script>
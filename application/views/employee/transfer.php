<div class="control-group">
<label class="control-label">Name:<span class="required">*</span></label>
<div class="controls">
    <input type="text" class="span6 m-wrap" data-required="1" id="value_name" name="value_name" autocomplete="off"><a id="namea" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
    <div id="name_list" class="help-block"></div>
</div> 
</div>
<script>
  function clickmeemp(x) {
    $("#value_name").val(x.innerHTML).attr("readonly", "readonly");
    $("#name_list").empty();
    $("#namea").removeClass("hidden");
}
$("#namea").click(function() {
    $("#value_name").val('').removeAttr("readonly");
    $("#namea").addClass("hidden");
});
$("#value_name").keyup(function(e){
    $("#name_list").empty();
    if($(this).val() != "" && $("#value_type").val() != "") {
        $.ajax({
            type:       "GET",
            url:        "<?php echo base_url(); ?>employee/"+getEmployeeType($("#value_type").val())+"/"+$(this).val(),
        //    timeout:    1000,
            success: function(data){
                var obj = $.parseJSON(data);
                if(obj.error != undefined) {
                    alert(obj.error);
                    setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                    return;
                }
                var ctr = obj.length;
                var emp_type_fields = $("#value_type").val();

                for (var i = 0; i <= ctr-1; i++) {
                        if (emp_type_fields == 0){
                            $("#name_list").append("<p class='help-block' onclick='clickmeemp(this)' data='"+obj[i].emp_code+"'>"+obj[i].emp_code+'-'+obj[i].emp_fname+' '+obj[i].emp_sname+"</p>");
                        }else if (emp_type_fields == 1){
                            $("#name_list").append("<p class='help-block' onclick='clickmeemp(this)' data='"+obj[i].mic_code+"'>"+obj[i].mic_code + '-' + obj[i].mic_name+"</p>");
                        }else if (emp_type_fields == 2){
                             $("#name_list").append("<p class='help-block' onclick='clickmeemp(this)' data='"+obj[i].sg_code+"'>"+obj[i].sg_code + '-' + obj[i].sg_name+"</p>");
                        }  
                }
            }
        });    
    }
});
</script>
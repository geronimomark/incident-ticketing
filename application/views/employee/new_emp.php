<div class="control-group">
    <label class="control-label">First Name<span class="required">*</span></label>
    <div class="controls">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_fname" name="value_fname" autocomplete="off">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Last Name<span class="required">*</span></label>
    <div class="controls">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_lname" name="value_lname" autocomplete="off">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Middle Initial<span class="required">*</span></label>
    <div class="controls">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_mi" name="value_mi" autocomplete="off">
    </div>
</div>
<div class="control-group">
    <label class="control-label">Short Name<span class="required">*</span></label>
    <div class="controls">
        <input type="text" class="span6 m-wrap" data-required="1" id="value_sname" name="value_sname" autocomplete="off">
    </div>
</div>
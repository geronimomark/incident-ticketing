<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Employee Maintenance</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
            <!-- BEGIN FORM-->
            <form  id="form_save" class="form-horizontal" action="<?=base_url().'employee/transfer_emp'?>" method="POST">
                <fieldset>
                    <?php if($this->session->flashdata("errorx")):?>
                    <div class="alert alert-error">
                        <button data-dismiss="alert" class="close"></button>
                        <?=$this->session->flashdata("errorx")?>
                    </div>
                    <?php endif ?>
                    <?php if($this->session->flashdata("successx")):?>
                    <div class="alert alert-success">
                        <button data-dismiss="alert" class="close"></button>
                        <?=$this->session->flashdata("successx")?>
                    </div>
                    <?php endif ?>
                    <div class="modal hide" id="myAlert" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Are you sure you want to save?</h3>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" id="save" data-dismiss="modal">Confirm</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                    <div class="modal hide" id="myAlert2" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Are you sure you want to delete?</h3>
                        </div>
                        <div class="modal-footer">
                            <a href="#" data="" class="btn btn-primary" id="delete" data-dismiss="modal">Confirm</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                
                        <!--Contents Here-->
                        <div class="control-group">
                        <label class="control-label">Action:<span class="required">*</span></label>
                        <div class="controls">
                            <select id="value_action" name="value_action" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <option value="0">NEW EMPLOYEE</option>
                                <option value="1">TRANSFER EMPLOYEE</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Type of Employee:<span class="required">*</span></label>
                        <div class="controls">
                            <select id="value_type" name="value_type" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <option value="0">CREW</option>
                                <option value="1">MIC/RIM</option>
                                <option value="2">SG</option>
                            </select>
                        </div>
                    </div>
                    <div id="details"></div>
                    <div id="name_search"></div>
                    <div class="control-group">
                    <label class="control-label">STORE:<span class="required">*</span></label>
                    <div class="controls">
                        <input type="text" class="span6 m-wrap" data-required="1" id="value_store" name="value_store" autocomplete="off"> <a id="storea" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
                        <div id="store_list" class="help-block"></div>
                    </div> 
                    </div>
                    <div class="form-actions">
                        <a class="btn btn-primary" data-toggle="modal" href="#myAlert" id="fake_submit">Update</a>
                        <button class="btn btn-primary hidden" id="update" type="submit"></button>
                        <button class="btn" type="button">Cancel</button>
                    </div>
                </fieldset>
            </form>
            <!-- END FORM-->
             </div>
        </div>
    </div>
<div class="block-content collapse in">
            <div class="span12">
            <!-- BEGIN FORM-->
            <form  id="form_edit" class="form-horizontal" action="<?=base_url().'employee/update'?>" method="POST">

                    <div class="modal hide" id="myAlert3" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Update this record</h3>
                        </div>
                        <br/>
                        <div class="row-fluid" id='view'></div>
                        <div class="modal-footer">
                            <a href="#" data="" class="btn btn-primary" id="edit" data-dismiss="modal">Update</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                        <button class="btn btn-primary hidden" id="edit_emp" type="submit"></button>
            </form>
            <!-- END FORM-->
             </div>
        </div>
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Employee List</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
            <!-- BEGIN FORM-->
            <form class="form-horizontal" action="<?=base_url().'employee/sort'?>" method="POST">
                <fieldset>
                  
                        <!--Contents Here-->
                        <div class="control-group">
                        <label class="control-label">Employee Type<span class="required">*</span></label>
                        <div class="controls">
                            <select id="value_stype" name="value_stype" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <option value="0">CREW</option>
                                <option value="1">MIC/RIM</option>
                                <option value="2">SG</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Search by:<span class="required">*</span></label>
                        <div class="controls">
                            <select id="value_searchby" name="value_searchby" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <option value="0">Name</option>
                                <option value="1">Store</option>
                               
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                    <label class="control-label">Key Word:<span class="required">*</span></label>
                    <div class="controls">
                        <input type="text" class="span6 m-wrap" data-required="1" id="value_kword" name="value_kword" autocomplete="off">
                    </div> 
                    </div>
                    <div class="form-actions"> 
                        <button class="btn btn-primary" id="sort" type="submit">SORT</button>
                        <button class="btn" type="button">Cancel</button>
                    </div>
                </fieldset>
            </form>
            <!-- END FORM-->
             </div>
        </div>
    </div>
   <?php if($_POST): ?>
<div class="row-fluid">
    <div class="span12">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left"><?=$search?></div>
                <div class="pull-right"><span class="badge badge-info"><?=count($data)?></span>
                </div>
            </div>
            <div class="block-content collapse in">
                 <input type="hidden" class="span6 m-wrap" id="search_type" value="<?=$type?>">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th> </th>
                            <th>EMPLOYEE CODE</th>
                            <th>NAME</th>
                            <th>STORE</th>
                        </tr>
                    </thead>
                      <tbody>
                       
                        <?php foreach ($data as $value):?>
                        <tr>
                            <td>
                                <a data-toggle="modal" data="<?=$value->$fields[0]?>" class="edit btn" title="Edit" href="#myAlert3"><i class="icon-edit"></i></a>
                                <a data-toggle="modal" data="<?=$value->$fields[0]?>" class="remove btn" title="Delete"  href="#myAlert2"><i class="icon-remove"></i></a>
                            </td>
                            <td><?=$value->$fields[0]?></td>
                            <td><?=$value->$fields[5]?>, <?=$value->$fields[3]?> <?=$value->$fields[4]?> </td>
                            <td><?=$value->$fields[2]?></td>
                        </tr>
                        <?php endforeach;?>
                   
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<?php endif?>
</div>
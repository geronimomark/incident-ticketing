<div class="row-fluid">
    <div class="span12">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">5 Recently Created UIR</div>
                <div class="pull-right"><span class="badge badge-info"><?=count($uir)?></span>

                </div>
            </div>
            <div class="block-content collapse in">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Ticket No.</th>
                            <th>UIR Class</th>
                            <th>Incident</th>
                            <th>Store</th>
                            <th>MIC</th>
                            <th>SG</th>
                            <th>Encode Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($uir as $value): ?>
                        <tr>
                            <td><?=$value->uir_ticket_no?></td>
                            <td><?=$value->main_name?></td>
                            <td><?=$value->incident_name?></td>
                            <td><?=$value->store_name?></td>
                            <td><?=$value->mic_name?></td>
                            <td><?=$value->sg_name?></td>
                            <td><?=$value->last_update?></td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Daily Statistics (<?=date("Y-m-d")?>)</div>
            <div class="pull-right"><span class="badge badge-warning"><?=$daily_count?></span>
            </div>
        </div>
        <div class="block-content collapse in">
            <table width="100%" >
                <tr >
            <?php $x = 0;?>
            <?php if($daily_count > 0):?>
            <?php $x = 0;?>
            <?php foreach ($daily as $key => $value): ?>
            <?php if($x == 3):?>
            <tr>
            <?php endif ?>
            <td>
            <div class="span4" style="width:100%;">
                <div  data-percent="<?=number_format($value->TOTAL/$daily_count*100, 2, '.', '')?>" class="chart easyPieChart" style="width: 110px; height: 110px; line-height: 110px;"><?=number_format($value->TOTAL/$daily_count*100, 2, ".", "")?>%<canvas height="110" width="110"></canvas></div>
                <div class="chart-bottom-heading"><span class="label label-info"><?=$value->main_name?> <span class="badge"><?=$value->TOTAL?></span></span>
                </div>
            </div>
            </td>
            <?php $x++;?>
            <?php if($x == 3):?>
            </tr>
            <?php $x=0;?>
            <?php endif ?>
             
            <?php endforeach ?>
            <?php endif ?>
       
            </table>
        </div>
    </div>
    <!-- /block -->
</div>
<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Monthly Statistics (<?=date("M")?>)</div>
            <div class="pull-right"><span class="badge badge-warning"><?=$month_count?></span>
            </div>
        </div>
        <div class="block-content collapse in">
             <table width="100%" >
                <tr >
            <?php $x = 0;?>
            <?php if($month_count > 0):?>
            <?php foreach ($month as $key => $value): ?>
          <?php if($x == 3):?>
            <tr>
            <?php endif ?>
            <td>
            <div class="span4" style="width:100%;">
                <div data-percent="<?=number_format($value->TOTAL/$month_count*100, 2, '.', '')?>" class="chart easyPieChart" style="width: 110px; height: 110px; line-height: 110px;"><?=number_format($value->TOTAL/$month_count*100, 2, ".", "")?>%<canvas height="110" width="110"></canvas></div>
                <div class="chart-bottom-heading"><span class="label label-info"><?=$value->main_name?> <span class="badge"><?=$value->TOTAL?></span></span>
                </div>
            </div>
                 </td>
            <?php $x++;?>
            <?php if($x == 3):?>
            </tr>
            <?php $x=0;?>
            <?php endif ?>
             
            <?php endforeach ?>
            <?php endif ?>
             </table>
        </div>
    </div>
    <!-- /block -->
</div>
<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Overall Statistics</div>
            <div class="pull-right"><span class="badge badge-warning"><?=$overall_count?></span>
            </div>
        </div>
        <div class="block-content collapse in">
               <table width="100%" >
                <tr >
            <?php $x = 0;?>
            <?php if($overall_count > 0):?>
            <?php foreach ($vtype as $key => $value): ?>
             <?php if($x == 3):?>
            <tr>
            <?php endif ?>
            <td>
            <div class="span4" style="width:100%;">
                <div data-percent="<?=number_format($value->TOTAL/$overall_count*100, 2, '.', '')?>" class="chart easyPieChart" style="width: 110px; height: 110px; line-height: 110px;"><?=number_format($value->TOTAL/$overall_count*100, 2, ".", "")?>%<canvas height="110" width="110"></canvas></div>
                <div class="chart-bottom-heading"><span class="label label-info"><?=$value->main_name?> <span class="badge"><?=$value->TOTAL?></span></span>
                </div>
            </div>
             </td>
            <?php $x++;?>
            <?php if($x == 3):?>
            </tr>
            <?php $x=0;?>
            <?php endif ?>
            <?php endforeach ?>
            <?php endif ?>
              </table>
        </div>
    </div>
    <!-- /block -->
</div>

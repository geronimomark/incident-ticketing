<script type="text/javascript" src="<?=base_url()?>bootstrap/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?=base_url()?>bootstrap/assets/form-validation.js"></script>
<link href="<?=base_url()?>bootstrap/vendors/datepicker.css" rel="stylesheet" media="screen">
<script src="<?=base_url()?>bootstrap/vendors/bootstrap-datepicker.js"></script>
<link href="<?=base_url()?>bootstrap/assets/timepicker.css" rel="stylesheet" media="screen">
<script src="<?=base_url()?>bootstrap/assets/timepicker.js"></script>
<script>
    jQuery(document).ready(function() {   
       FormValidation.init();
    });

    $(".datepicker").datepicker({format: "yyyy-mm-dd"}).attr("readonly", "readonly");

    $('#time').timepicker();
    
    function clickme(x) {
        $("#rm_mic").val(x.innerHTML).attr("readonly", "readonly");
        $("#rm_mic_list").empty();
        $("#mica").removeClass("hidden");
    }

    function clickmesg(x) {
        $("#sg").val(x.innerHTML).attr("readonly", "readonly");
        $("#sg_list").empty();
        $("#sga").removeClass("hidden");
    }

    function clickmest(x) {
        $("#store").val(x.innerHTML).attr("readonly", "readonly");
        $("#store_list").empty();
        $("#sta").removeClass("hidden");
        $("#rm_mic").val("").removeAttr("readonly");
        $("#sg").val("").removeAttr("readonly");
    }

    $("#mica").click(function() {
        $("#rm_mic").val('').removeAttr("readonly");
        $("#mica").addClass("hidden");
    });

    $("#sga").click(function() {
        $("#sg").val('').removeAttr("readonly");
        $("#sga").addClass("hidden");
    });

    $("#sta").click(function() {
        $("#store").val('').removeAttr("readonly");
        $("#sta").addClass("hidden");
        $("#mica").trigger("click");
        $("#sga").trigger("click");
    });

    $("#uir_class").change(function() {
        if($(this).val() != "") {
            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>uir_new/get_sub/"+$(this).val()+"/"+$("#store").val(),
               // timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    var ctr = obj.length;
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    $("#incident").empty().append("<option value=''>---Select Incident---</option>");
                    for (var i = 0; i <= ctr-1; i++) {
                        $("#incident").append("<option data='"+obj[i].uir_code+"|"+obj[i].security_code+"' value='"+obj[i].incident_code+"'>"+obj[i].incident_name+"</option>");
                    }
                }
            });    
        } else {
            $("#incident").empty().append("<option value=''>---Select Incident---</option>").trigger("change");
        }
    });

    $("#incident").change(function() {
        if($(this).val() != "") {
            var data = $("#incident option:selected").attr("data");
            var x = data.split("|");
            $("#uir_code_value").val(x[0]);
            $("#sec_code_value").val(x[1]).css("background-color", x[1]);
            $("#uir_code").removeClass("hidden");
            $("#sec_code").removeClass("hidden");
            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>uir_new/get_details/"+x[0],
               // timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    $("#details").empty().append(obj.data);
                }
            });
        } else {
            $("#uir_code").addClass("hidden");
            $("#sec_code").addClass("hidden").css("background-color", "white");
        }
    });

    $("#rm_mic").keyup(function(e){
        $("#rm_mic_list").empty();
        if($(this).val() != "") {
            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>uir_new/get_mic/"+$(this).val()+"/"+$("#store").val(),
               // timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    var ctr = obj.length;
                    for (var i = 0; i <= ctr-1; i++) {
                        $("#rm_mic_list").append("<p class='help-block' onclick='clickme(this)' data='"+obj[i].mic_code+"'>"+obj[i].mic_code + '-' + obj[i].mic_name+"</p>");
                    }
                }
            });    
        }
    });

    $("#sg").keyup(function(e){
        $("#sg_list").empty();
        if($(this).val() != "") {
            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>uir_new/get_sg/"+$(this).val()+"/"+$("#store").val(),
             //   timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    var ctr = obj.length;
                    for (var i = 0; i <= ctr-1; i++) {
                        $("#sg_list").append("<p class='help-block' onclick='clickmesg(this)' data='"+obj[i].sg_code+"'>"+obj[i].sg_code + '-' + obj[i].sg_name+"</p>");
                    }
                }
            });    
        }
    });

    $("#store").keyup(function(e){
        $("#store_list").empty();
        if($(this).val() != "") {
            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>uir_new/get_store/"+$(this).val(),
           //     timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    var ctr = obj.length;
                    for (var i = 0; i <= ctr-1; i++) {
                        $("#store_list").append("<p class='help-block' onclick='clickmest(this)' data='"+obj[i].store_code+"'>"+obj[i].store_code + '-' + obj[i].store_name + '-' + obj[i].name_code + '-' + obj[i].pc_code +"</p>");
                    }
                }
            });    
        }
    });

    $("#save").click(function() {
        $("#submit").trigger("click");
    });
    $("#fake_submit2").click(function() {
        $("#action_form").attr("action","<?php echo base_url();?>uir_new/save_only");
    }); $("#fake_submit").click(function() {
        $("#action_form").attr("action","<?php echo base_url();?>uir_new/save");
    });
</script>
<script type="text/javascript" src="<?=base_url()?>bootstrap/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?=base_url()?>bootstrap/assets/form-validation.js"></script>
<link href="<?=base_url()?>bootstrap/vendors/datepicker.css" rel="stylesheet" media="screen">
<script src="<?=base_url()?>bootstrap/vendors/bootstrap-datepicker.js"></script>
<link href="<?=base_url()?>bootstrap/assets/timepicker.css" rel="stylesheet" media="screen">
<script src="<?=base_url()?>bootstrap/assets/timepicker.js"></script>
<script>
    jQuery(document).ready(function() {   
       FormValidation.init();
    });

    $(".datepicker").datepicker({format: "yyyy-mm-dd"}).attr("readonly", "readonly");
    
       function clickmest(x) {
        $("#store").val(x.innerHTML).attr("readonly", "readonly");
        $("#store_list").empty();
        $("#sta").removeClass("hidden");
       
       
    }

    $("#date").click(function(){
  		$("#year").attr('disabled','disabled');
       	$("#month").attr('disabled','disabled');
       	$("#year option").filter(function(){
	     return $(this).val() == '';
	        }).prop("selected",true);
       	$("#month option").filter(function(){
	     return $(this).val() == '';
	        }).prop("selected",true);
    });

     $("#store").keyup(function(e){
        $("#store_list").empty();
        if($(this).val() != "") {
            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>uir_new/get_store/"+$(this).val(),
           //     timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    var ctr = obj.length;
                    for (var i = 0; i <= ctr-1; i++) {
                        $("#store_list").append("<p class='help-block' onclick='clickmest(this)' data='"+obj[i].store_code+"'>"+obj[i].store_code + '-' + obj[i].store_name + '-' + obj[i].name_code + '-' + obj[i].pc_code +"</p>");
                    }
                }
            });    
        }
    });

	$("#sta").click(function() {
        $("#store").val('').removeAttr("readonly");
        $("#sta").addClass("hidden");
       
    });

	$("#clr_date").click(function() {
		$("#date").val('');
		$("#year").removeAttr('disabled');
	    $("#month").removeAttr('disabled');
    });

	$("#save").click(function(){
		$("#submit").trigger("click");
	});

	$("#uir_class").change(function() {
       load_incident($(this).val());
    });

    function load_incident(data){
     if(data != "") {
            $.ajax({
                type:       "GET",
                url:        "<?php echo base_url(); ?>uir_new/get_sub/"+data,
               // timeout:    1000,
                success: function(data){
                    var obj = $.parseJSON(data);
                    var ctr = obj.length;
                    if(obj.error != undefined) {
                        alert(obj.error);
                        setTimeout(function(){window.location.href = '<?php echo base_url();?>';},1000);  
                        return;
                    }
                    $("#incident").empty().append("<option value=''>---Select Incident---</option>");
                    for (var i = 0; i <= ctr-1; i++) {
                        $("#incident").append("<option data='"+obj[i].uir_code+"|"+obj[i].security_code+"' value='"+obj[i].incident_code+"'>"+obj[i].incident_name+"</option>");
                    }
                }
            });    
        } else {
            $("#incident").empty().append("<option value=''>---Select Incident---</option>").trigger("change");
        }
}

</script>
<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Extract Data</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
            <!-- BEGIN FORM-->
            <form class="form-horizontal" action="<?=base_url()?>extract/generate_excel" method="POST">
                <fieldset>
                    <div class="modal hide" id="myAlert" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Are you sure you want to generate this report?</h3>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" id="save" data-dismiss="modal">Confirm</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="date" class="control-label">Date of Discovery<span class="required">*</span></label>
                        <div class="controls">
                        <input type="text" value="" id="date" name="date" class="input-xlarge datepicker"> <a id="clr_date" class="btn btn-inverse"><i class="icon-refresh icon-white"></i></a>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">SELECT YEAR:<span class="required">*</span></label>
                        <div class="controls">
                            <select id="year" name="year" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <option value="<?=date('Y')?>"><?=date('Y')?></option>
                                <option value="<?=date('Y')-1?>"><?=date('Y')-1?></option>
                                <option value="<?=date('Y')-2?>"><?=date('Y')-2?></option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">SELECT MONTH:<span class="required">*</span></label>
                        <div class="controls">
                            <select id="month" name="month" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                    </div>
           
                    <div class="control-group">
                        <label class="control-label">UIR CLASS:</label>
                        <div class="controls">
                            <select id="uir_class" name="uir_class" class="span6 m-wrap">
                                <option value="">---Select---</option>
                                <?php foreach ($uir_class as $key => $value):?>
                                <option value="<?=$value->main_code?>"><?=$value->main_name?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">INCIDENT:</label>
                        <div class="controls">
                            <select id="incident" name="incident" class="span6 m-wrap">
                                <option value="">---Select---</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">STORE NAME:</label>
                        <div class="controls">
                            <input type="text" class="span6 m-wrap" data-required="1" id="store" name="store" autocomplete="off"> <a id="sta" class="btn btn-inverse hidden"><i class="icon-refresh icon-white"></i></a>
                            <div id="store_list" class="help-block"></div>
                        </div>
                    </div>
                    <div class="form-actions">
                    	<a href="#myAlert" data-toggle="modal" class="btn btn-primary">Extract <i class="icon-download-alt icon-white"></i></a>
                        <button class="btn btn-primary hide" id="submit" type="submit">Search</button>
                        <button class="btn" type="button">Cancel</button>
                    </div>
                </fieldset>
            </form>
            <!-- END FORM-->
            </div>
        </div>
    </div>
    <!-- /block -->
</div>
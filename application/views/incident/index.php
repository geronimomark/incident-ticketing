<div class="row-fluid">
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">Sort Incident</div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
            <!-- BEGIN FORM-->
          
                <fieldset>
                    <form  id="form_update"class="form-horizontal" action="<?=base_url()?>incident/update_incident" method="POST">
                    <div class="modal hide" id="myAlert" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Are you sure you want to delete this record?</h3>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" id="delete" data-dismiss="modal">Confirm</a>
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>

                    <div class="modal hide" id="myAlert2" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Modify Incident Information</h3>
                        </div>
                <div class="row-fluid" id='view'></div>
                      
                        <div class="modal-footer">
                             <button class="btn btn-primary" id="submit" type="submit">Update</button>
                             <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                            
                        </div>
                    </div>
                     <div class="modal hide" id="myAlert3" style="display: none;" aria-hidden="true">
                        <div class="modal-header">
                            <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                            <h3>Are you sure you want to delete this UIR?</h3>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-primary" id="delete_yes" data-dismiss="modal">Confirm</a>
                            <a href="#" class="btn" data-dismiss="modal">No</a>
                        </div>
                    </div>

                    </form>
                    <form class="form-horizontal" action="<?=base_url()?>incident/sort" method="POST">
                    <div class="control-group">
                        <label class="control-label">CLASS NAME:</label>
                        <div class="controls">
                         
                        <select id="class_name" name="class_name" class="span6 m-wrap" >
                              <?php if($this->input->post('class_name') !=""): ?>
                               <?php $str = $this->input->post('class_name'); ?>    
                               <option value="">---Select---</option>
                                    <!--To select defaul value from wht user select in sorting incident record-->
                                    <?php foreach ($class_name as $key => $value):?>
                                    <?php if($str == $value->main_code): ?>
                                     <option value="<?=$value->main_code?>" selected="selected"><?=$value->main_name?></option>
                                    <?php else: ?>
                                     <option value="<?=$value->main_code?>"><?=$value->main_name?></option>
                                    <?php endif; ?>
                                    <?php endforeach;?>

                              <?php else: ?>
                                    <option value="">---Select---</option>
                                    <?php foreach ($class_name as $key => $value):?>
                                    <option value="<?=$value->main_code?>"><?=$value->main_name?></option>
                                    <?php endforeach;?>
                              <?php endif ?>
                                
                            
                     </select>
                        </div>
                    </div>
              
           
    
         
                    <div class="form-actions">
                        <button class="btn btn-primary" id="submit" type="submit">Sort</button>
                       
                        <!-- to open modal -->
                        <a id="trigger_delete" href="#myAlert" data-toggle="modal" class="hide btn btn-inverse"><i class="icon-envelope icon-white"></i></a>
                        <a id="trigger_edit" href="#myAlert2" data-toggle="modal" class="hide btn btn-inverse"><i class="icon-envelope icon-white"></i></a>
                    </div>
                    </form>
                </fieldset>
       
            <!-- END FORM-->
            </div>
        </div>
    </div>
    <!-- /block -->
         <div class="block">
            <div class="navbar navbar-inner block-header">
           
                      
                <div class="muted pull-left">Incidents Information Maintenance</div>
        
                <div class="pull-right"><span class="badge badge-info">0</span>

                </div>
           
            </div>
      

            <div class="block-content collapse in">
                <table class="table table-striped" id="table_incident">
                    <thead>
                        <tr>
                            <th> </th>
                            <th>INCIDENT NAME</th>
                            <th>SECURITY CODE</th>
                        </tr>
                        <tbody>
                           
                            <?php if($this->input->post('class_name') !=""): ?>
                          
                            <?php foreach ($incident as $value): ?>                  
                            <tr>
                            <td>
                <a data-toggle="modal" data="<?=$value->incident_code ?>" class="edit_me btn"><i class="icon-edit"></i></a>
                <a data-toggle="modal" data="<?=$value->incident_code ?>" class="delete_me btn btn-inverse"><i class="icon-trash icon-white"></i></a>
            </td>
            <td><?=$value->incident_name ?></td>
            <td><span style="color:<?=$value->security_code ?>"><?=$value->security_code ?></span></td>
            </tr>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                    </thead>
                </table>
            </div>
        </div>
</div>

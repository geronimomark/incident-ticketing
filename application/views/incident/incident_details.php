<div class="block">
    <div class="control-group">
        <label class="control-label">INCIDENT NAME<span class="required">*</span></label>
        <div class="controls">
            <?php foreach($incidents as $key => $value) :?>
            <input type="text" class="span10 m-wrap" data-required="1" id="incident_name_update" name="incident_name_update" autocomplete="off" value="<?=$value->incident_name ?>">
           
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">CLASS NAME:</label>
        <div class="controls">
         
        <select id="class_name" name="class_name" class="span10 m-wrap" >
             
               <option value="1">---Select---</option>
            <?php foreach($vtype as $key => $value2) : ?>
                <?php if($value->vtype_main_code == $value2->main_code) : ?>
                <option value="<?=$value2->main_code ?>" selected="selected"><?=$value2->main_name ?></option>
                <?php else : ?>
                <option value="<?=$value2->main_code ?>"><?=$value2->main_name ?></option>
                <?php endif ?>
        
            <?php endforeach ?>

        
                                        
         </select>
        </div>
    </div>
        <div class="control-group">
        <label class="control-label">SECURITY CODE:</label>
        <div class="controls">
         
        <select id="sec_code" name="sec_code" class="span10 m-wrap" style="background-color:<?=$value->security_code ?>" >
             
               <option value="1">---Select---</option>
            <?php foreach($sec_code as $key => $value2) : ?>
                <?php if($value->security_code == $value2->security_code) : ?>
                <option value="<?=$value2->security_code ?>" selected="selected"><?=$value2->security_code ?></option>
                <?php else : ?>
                <option value="<?=$value2->security_code ?>" ><?=$value2->security_code ?></option>
                <?php endif ?>
        
            <?php endforeach ?>

       
                                        
         </select>
        </div>
    </div>
     <?php endforeach ?>
</div>
<script type="text/javascript">
$("#sec_code").change(function(){
$(this).css("background-color", $(this).val());
});

</script>